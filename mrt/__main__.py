from mrt.MRT import multi_reclass_tool
import argparse

parser = argparse.ArgumentParser(
    prog="MRT+",
    description="█▓▒░ WENR Multi Reclass Tool ░▒▓█",
    epilog="|WUR|WENR|B&B|HR|november2023|CC BY-NC-ND 4.0|👌|",
)
parser.add_argument("xlsx", help="path to Excel file with reclass rules", type=str)
parser.add_argument("raster", help="path and filename to combined raster", type=str)
parser.add_argument("us", help="name of upstream Excel sheet", type=str)

parser.add_argument("--ds_sheet", help="name of downstream Excel sheet", type=str)
parser.add_argument(
    "--ds_xlsx",
    help="path to Excel file with downstream sheet (defaults to `xlsx`)",
    type=str,
)

parser.add_argument("--report", action="store_true", help="write report to file")
parser.add_argument("--out_dir", help="output directory (default ./)", default="./")
parser.add_argument(
    "--basename",
    help="output file basename (default: Reclass Excel filename)",
    type=str,
)
parser.add_argument(
    "--ts",
    help="add timestamp to basename",
    action="store_true",
)
write_group = parser.add_argument_group("--write_raster")
write_group.add_argument(
    "--target",
    help="write what to raster?",
    type=str,
)
write_group.add_argument(
    "--of",
    help="output file format (default: geotiff)",
    choices=["GTiff", "Ehdr"],
    type=str,
    default="GTiff",
)
write_group.add_argument(
    "--xtnt",
    help="target extent: left bottom right top (default: equal to combined raster)",
    type=int,
    nargs=4,
)
write_group.add_argument(
    "--tr",
    help="target resolution in m (default: equal to combined raster)",
    type=float,
)
write_group.add_argument(
    "--r",
    help="resample method (default: mode)",
    choices=["mode", "nearest"],
    type=str,
    default="mode",
)
write_group.add_argument(
    "--nodata", help="set a value as NoData (default: 0)", default=0, type=int
)
write_group.add_argument(
    "--mnp_legend", help="write MNP style CSV table", action="store_true"
)
write_group.add_argument(
    "--testing",
    help="test configuration, write 20 blocks to file",
    action="store_true",
)
args = parser.parse_args()

print("\n\n***Starting Multi Reclass Tool***\n\n")
# Run the model and time it
multi_reclass_tool(parameter_dict=vars(args))
print("done ✨ 🍰 ✨")
