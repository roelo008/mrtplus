"""
Multi Reclass Tool
Hans Roelofsen, june 2023

"""

from typing import List
import datetime
import os.path
import time
import rasterio.drivers


from mrt.preparation.input_pathway import verify_input_pathway
from mrt.preparation.output_pathway import (
    verify_output_pathway,
    update_parameter_dict,
)
from mrt.upstream.combined_raster import get_combined_raster
from mrt.upstream.reclass_rules import get_reclass_rules
from mrt.downstream.reclassifiers import (
    ReClassifier,
    build_downstream_reclassifiers,
    evaluate_reclassifiers,
    build_default_reclassifiers,
)
from mrt.outputs.report import write_report
from mrt.outputs.raster import write_raster
from mrt.outputs.sidecars import build_side_cars


def verify_and_verbose_arguments(parameter_dict: dict) -> dict:
    """ "
    Add verbose keys to parameter dict, and also include basename and timestamp

    """

    # If downstream Excel, but not Downstream Sheet, raise error
    if parameter_dict["ds_xlsx"] and not parameter_dict["ds_sheet"]:
        raise ValueError("Please provide downstream sheet name.")

    # Default downstream excel to upstream excel
    if parameter_dict["ds_sheet"] and not parameter_dict["ds_xlsx"]:
        parameter_dict["ds_xlsx"] = parameter_dict["xlsx"]

    # Set timestamp
    parameter_dict["mrt_timestamp"] = datetime.datetime.now()

    # Create basename
    basename = (
        os.path.basename(parameter_dict["xlsx"]).replace(".", "")
        if not parameter_dict["basename"]
        else parameter_dict["basename"]
    )
    if parameter_dict["ts"]:
        basename = (
            f'{basename}_{parameter_dict["mrt_timestamp"].strftime("%Y%m%d-%H%M%S")}'
        )
    parameter_dict["basename"] = basename

    # Set more verbose names
    parameter_dict["upstream_excel"] = parameter_dict["xlsx"]
    parameter_dict["combined_raster"] = parameter_dict["raster"]
    parameter_dict["upstream_sheet"] = parameter_dict["us"]
    parameter_dict["downstream_excel"] = parameter_dict["ds_xlsx"]
    parameter_dict["downstream_sheet"] = parameter_dict["ds_sheet"]
    parameter_dict["resample"] = parameter_dict["r"]

    # Set output raster filename extension
    parameter_dict["raster_extension"] = {
        j: i for i, j in rasterio.drivers.raster_driver_extensions().items()
    }[parameter_dict["of"]]

    return parameter_dict


def multi_reclass_tool(parameter_dict: dict):
    start = time.time()

    parameter_dict = verify_and_verbose_arguments(parameter_dict)

    verify_input_pathway(arguments=parameter_dict)
    output_pathway = verify_output_pathway(arguments=parameter_dict)

    parameter_dict = update_parameter_dict(parameter_dict, output_pathway)

    combined_raster = get_combined_raster(arguments=parameter_dict)

    reclass_rules = get_reclass_rules(
        arguments=parameter_dict, combined_raster=combined_raster
    )

    reclassifiers: List[ReClassifier] = build_default_reclassifiers(reclass_rules)

    if parameter_dict["downstream_sheet"] or parameter_dict["firstrule_models"]:
        downstream_reclassifiers: List[ReClassifier] = build_downstream_reclassifiers(
            reclassifiers=reclassifiers,
            arguments=parameter_dict,
            reclass_rules=reclass_rules,
        )
        reclassifiers += downstream_reclassifiers

    reclassifiers = evaluate_reclassifiers(
        reclassifiers=reclassifiers, combined_raster=combined_raster
    )

    if parameter_dict["report"]:
        write_report(
            arguments=parameter_dict,
            reclass_rules=reclass_rules,
            combined_raster=combined_raster,
            reclassifiers=reclassifiers,
        )

    if parameter_dict["target"]:
        write_raster(
            arguments=parameter_dict,
            reclassifiers=reclassifiers,
            combined_raster=combined_raster,
        )
        build_side_cars(arguments=parameter_dict, reclassifiers=reclassifiers)

    print(f"run took {time.time() - start} seconds")
