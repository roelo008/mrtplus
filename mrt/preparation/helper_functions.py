import geopandas as gp
import pandas as pd
import os
from typing import List

MANDATORY_HEADER_OVERVIEW = {
    "upstream": {
        "newvalue",
        "description",
        "remark",
        "volgnummer",
        "reclass",
    },
    "downstream": {
        "newvalue",
        "description",
    },
    "combined_vat": {
        "Value",
        "Count",
    },
    "downstream_model_specification": {"Value", "Description"},
}


def read_raster_vat(src: str, sample: bool = False) -> pd.DataFrame:
    """
    Read a raster attribute table, either as:
    <raster_file_path>.tif.vat.dbf
    <raster_file_path>.csv

    :param src: path to raster attribute table
    :param sample: read only 10 rows
    :return: pandas dataframe
    """

    csv = f"{os.path.splitext(src)[0]}.csv"
    dbf = f"{src}.vat.dbf"

    if os.path.isfile(csv):
        vat = pd.read_csv(csv, nrows=10 if sample else None)
    elif os.path.isfile(dbf):
        vat = gp.read_file(dbf, geometry=False, rows=10 if sample else None)
    else:
        raise ValueError(f"{src} does not have valid raster VAT.")
    return vat


def parse_rule(rule: str | int, category: str | None = None) -> str | List[int] | None:
    """
    Parse rule information in reclass df to a list of values for a category a

    INPUT    OUTPUT
    1        "a in [1]"
    "1"      "a in [1]"
    "1,2,3"  "a in [1, 2, 3]"
    "-1"     "a not in [1]"
    "-1,2"   "a not in [1, 2]"
    "n"      None
    "10*15"  "a in [10, 11, 12, 13, 14, 15]"
    "-10*15" "a not in [10, 11, 12, 13, 14, 15]"
    else     Exception

    :param rule: string or int
    :param category: name source raster, used to negate number(s) starting with "-". Default None
    :param negate: return negation of provided numbers if txt.startswith('-'). Default True
    :return: see above.
    """

    negate = False
    value_range = False
    seperator = ","

    assert isinstance(rule, (int, str)), "Unexpected reclass rule found: {0}".format(
        rule
    )

    if isinstance(rule, int):
        if rule < 0:
            negate = True
        values = [abs(rule)]

    else:
        rule = rule.strip(", ")

        if rule == "n":
            return None

        if rule.startswith("-"):
            negate = True
            rule = rule[1:]

        if "*" in rule:
            value_range = True
            seperator = "*"

        try:
            values = [int(v.strip()) for v in rule.split(seperator)]
            if value_range:
                if len(values) > 2:
                    raise AssertionError("Provide only start-stop values when using *.")
                values = [i for i in range(values[0], values[1] + 1)]
        except ValueError as e:
            raise AssertionError(
                "Unexpected value found in reclass rule {0}: {1}".format(rule, e)
            )

    if category:
        return "`{0}` {1} {2}".format(category, "not in" if negate else "in", values)
    else:
        return values
