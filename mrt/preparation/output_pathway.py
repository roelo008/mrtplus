from pathlib import Path
import logging
import pandas as pd
import rasterio as rio

from mnp.utils import get_logger, log_start_completed
from mrt.preparation.helper_functions import MANDATORY_HEADER_OVERVIEW

from model_framework.verification_procedures import VerificationProcedure
from model_framework.input_verification_functions import (
    table_coupled_one_to_one,
    are_are_not,
    does_does_not,
    is_is_not,
    table_has_headers,
    is_directory,
)

from model_framework.io_pathways import OutputPathway


class ValidWriteRequest(VerificationProcedure):
    """
    Verificationprocedure to check if request to write a raster is valid
    """

    def __init__(self, arguments):
        super().__init__()
        self.procedure_name = "valid_write_request"
        self.arguments = arguments

    def run(self):
        valid_write_targets = ["newvalue", "firstrule"]

        # Append with firstrule-based downstream models
        valid_write_targets += [
            c
            for c in list(
                pd.read_excel(
                    io=self.arguments["upstream_excel"],
                    sheet_name=self.arguments["upstream_sheet"],
                )
            )
            if c.startswith("_")
        ]

        #
        if self.arguments["ds_sheet"]:
            valid_write_targets += [
                c
                for c in list(
                    pd.read_excel(
                        io=self.arguments["downstream_excel"],
                        sheet_name=self.arguments["downstream_sheet"],
                    )
                )
                if c not in ["newvalue", "description"]
            ]

        check = self.arguments["target"] in valid_write_targets
        self.checks.append(
            (
                check,
                f"{self.arguments['target']} {is_is_not[check]} a valid write target",
            )
        )


class ValidDownstreamModelDesigns(VerificationProcedure):
    """ "
    Verificationprocedure to check if user-specifications of a downstream model are valid.
    This applies only to downstreammodels that have a seperate Excel-sheet to specify the numerical
    values corresponding to their output categories. Checks are

    1. this sheet has valid headers
    2. values-descriptions are coupled 1:1
    3.
    """

    def __init__(self, arguments: dict):
        super().__init__()
        self.procedure_name = "valid_downstream_model_designs"
        self.arguments = arguments
        self.downstream_models = []

    def run(self):
        # Gather downstream models from the upstream sheet.
        self.downstream_models.extend(
            [
                c
                for c in list(
                    pd.read_excel(
                        io=self.arguments["upstream_excel"],
                        sheet_name=self.arguments["upstream_sheet"],
                    )
                )
                if c.startswith("_")
            ]
        )

        # Gather downstreammodels from the downstream sheet
        if self.arguments["downstream_sheet"]:
            self.downstream_models.extend(
                [
                    c
                    for c in list(
                        pd.read_excel(
                            io=self.arguments["downstream_excel"],
                            sheet_name=self.arguments["downstream_sheet"],
                        )
                    )
                    if c not in ["newvalue", "description"]
                ]
            )

        if self.downstream_models:
            for model in self.downstream_models:
                key = "firstrule" if model[0] == "_" else "newvalue"
                src = {
                    "firstrule": {
                        "excel": self.arguments["upstream_excel"],
                        "sheet": self.arguments["upstream_sheet"],
                    },
                    "newvalue": {
                        "excel": self.arguments["downstream_excel"],
                        "sheet": self.arguments["downstream_sheet"],
                    },
                }[key]

                # Try if the excel contains a sheet named as the model. If not, proceed to next model
                try:
                    _ = pd.read_excel(src["excel"], sheet_name=model)
                except ValueError:
                    continue

                # Check for valid headers
                check, message = table_has_headers(
                    path=Path(src["excel"]),
                    mandatory_headers=MANDATORY_HEADER_OVERVIEW[
                        "downstream_model_specification"
                    ],
                    sheet_name=model,
                )
                self.checks.append(
                    (
                        check,
                        message.replace("<file_name>", f"{src['excel']} sheet {model}"),
                    )
                )

                if self.checks[-1][0]:
                    # If valid headers, proceed to next check: Value-Descriptions are coupled 1:1
                    check, message = table_coupled_one_to_one(
                        path=Path(src["excel"]),
                        column_name1="Value",
                        column_name2="Description",
                        sheet_name=model,
                    )
                    self.checks.append((check, message))

                if self.checks[-1][0]:
                    parent_df = pd.read_excel(src["excel"], src["sheet"])
                    parent_values = parent_df.loc[:, model].dropna()
                    check = set(parent_df.Description) == set(parent_values)
                    self.checks.append(
                        (
                            check,
                            f"Downstream values for model {model} in seperate sheet {does_does_not[check]} match values in parent sheet",
                        )
                    )
        else:
            self.checks.append((True, "No downstream models found"))


class ValidOutputDirectory(VerificationProcedure):
    """
    Verificationprocedure for valid output directory.
    """

    def __init__(self, arguments):
        super().__init__()
        self.procedure_name = "valid_output_directory"
        self.arguments = arguments

    def run(self):
        self.checks.append(is_directory(Path(self.arguments["out_dir"])))


class ValidWriteDesign(VerificationProcedure):
    """ "
    Verify that specifications for output rasters are valid. Checks are:

    1. target resolution is commensurable with combined-raster resolution
    2. target resolution and bounds are conmensurable with target shpe
    3. target bounds are not disjoint with combined raster bounds
    """

    def __init__(self, arguments: dict):
        super().__init__()
        self.arguments = arguments

    def run(self):
        combined_resolution = getattr(
            rio.open(self.arguments["combined_raster"]), "res"
        )[0]
        combined_bounds = getattr(rio.open(self.arguments["combined_raster"]), "bounds")

        target_resolution = (
            self.arguments["tr"] if self.arguments["tr"] else combined_resolution
        )

        target_bounds = (
            rio.coords.BoundingBox(*self.arguments["xtnt"])
            if self.arguments["xtnt"]
            else combined_bounds
        )

        # Is target resolution a multitude of combined resolution?
        check = target_resolution % combined_resolution == 0
        self.checks.append(
            (
                check,
                f"Target resolution {target_resolution} {is_is_not[check]} a multitude of source resolution {combined_resolution}.",
            )
        )

        # Do target resolution and bounds lead to integer number of columns?
        target_columns = (target_bounds.right - target_bounds.left) / target_resolution
        check = target_columns % 1 == 0
        self.checks.append(
            (
                check,
                "Target bounds and resolution give {0} column count in target raster: {1}".format(
                    {True: "integer", False: "non-integer"}[check], target_columns
                ),
            )
        )

        # Do target resolution and bounds lead to integer number of rows?
        target_rows = (target_bounds.top - target_bounds.bottom) / target_resolution
        check = target_rows % 1 == 0
        self.checks.append(
            (
                check,
                "Target bounds and resolution give {0} row count in target raster: {1}".format(
                    {True: "integer", False: "non-integer"}[check], target_rows
                ),
            )
        )

        # Are target bounds disjoint from combined bounds?
        check = not rio.coords.disjoint_bounds(combined_bounds, target_bounds)
        self.checks.append(
            (
                check,
                f"Target bounds {are_are_not[check]} (at least partly) within source bounds.",
            )
        )


class MRTOutputPathway:
    def __init__(self, arguments: dict):
        self.has_firstrule_models = False
        self.procedures = []
        self.arguments = arguments

    def identify(
        self,
    ):
        """
        Identify the users design of the output pathway
        Parameters
        ----------
        arguments

        Returns
        -------

        """

        self.procedures.append(ValidDownstreamModelDesigns(arguments=self.arguments))

        if self.arguments["out_dir"]:
            # Output directory must exist
            self.procedures.append(ValidOutputDirectory(arguments=self.arguments))

        if self.arguments["target"]:
            # Target to write must be either 'newvalue', 'firstrule' or a Downstream model
            self.procedures.append(ValidWriteRequest(arguments=self.arguments))

        if self.arguments["tr"] or self.arguments["xtnt"]:
            # If target resolution and/or extent are provided, verify a valid target raster desigin
            self.procedures.append(ValidWriteDesign(arguments=self.arguments))

    def verify(self):
        """
        Update and check status of all input verification procedures. Output all messages to log.
        """

        # Execute all compiled procedures
        for procedure in self.procedures:
            procedure.run()
            procedure.update_status()

        # Gather status of each procedure
        procedure_results = []
        for procedure in self.procedures:
            procedure_results.append(getattr(procedure, "status"))

        # Output to log
        self.log_messages()

        # If any procedure has failed, raise RuntimeError to end flow
        if all(procedure_results):
            self.all_checks_passed = True
        else:
            message = (
                "Cannot proceed due to errors in user input. See Errors.txt log file."
            )
            raise RuntimeError(message)

    def log_messages(self):
        logger = get_logger()
        logger.setLevel(logging.INFO)
        for procedure in self.procedures:
            for passed, message in procedure.checks:
                if passed:
                    logger.info(message)
                else:
                    logger.error(message)

    def identify_firstrule_models(self):
        """

        Returns
        -------

        """
        for procedure in self.procedures:
            if hasattr(procedure, "downstream_models"):
                setattr(
                    self,
                    "has_firstrule_models",
                    any([x.startswith("_") for x in procedure.downstream_models]),
                )


@log_start_completed
def verify_output_pathway(arguments: dict):
    """

    Parameters
    ----------
    arguments:


    Returns
    -------

    """
    output_pathway = MRTOutputPathway(arguments=arguments)
    output_pathway.identify()
    output_pathway.verify()
    output_pathway.identify_firstrule_models()
    return output_pathway


def update_parameter_dict(arguments: dict, output_pathway: OutputPathway):
    """
    Update the parameter dictionary with information that is obtained from the output pathway:

    Parameters
    ----------
    arguments
    output_pathway

    Returns
    -------

    """
    arguments["firstrule_models"] = output_pathway.has_firstrule_models
    return arguments
