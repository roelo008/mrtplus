import logging
from pathlib import Path
import pandas as pd

from mnp.utils import get_logger, log_start_completed

from model_framework.verification_procedures import (
    VerificationProcedure,
    VerifyExcel,
    VerifyIsGeospatialRaster,
)

from model_framework.input_verification_functions import (
    table_coupled_one_to_one,
    are_are_not,
    does_does_not,
)

from model_framework.io_pathways import InputPathway

from mrt.preparation.helper_functions import (
    MANDATORY_HEADER_OVERVIEW,
    read_raster_vat,
    parse_rule,
)


class VerifyUpstreamExcelContents(VerificationProcedure):
    def __init__(self, arguments: dict):
        super().__init__()
        self.arguments = arguments

    def run(self):
        """
        Verify that an upstream Excel sheet is correctly formatted and contains all relevant columns and values in it.
        The checks are:

        1. upstream sheet has correct headers
        2. nevalue and description columns are coupled 1:1
        3. Reclass rules do not contain missing values
        4. All source-rasters in the upstream sheet are present in the combined raster VAT
        5. Values employed in reclass rules are subset of values in the combined raster VAT

        Returns
        -------

        """

        # Check if newvalues and descriptions are coupled 1:1?
        check, message = table_coupled_one_to_one(
            path=Path(self.arguments["upstream_excel"]),
            column_name1="newvalue",
            column_name2="description",
            sheet_name=self.arguments["upstream_sheet"],
        )
        self.checks.append(
            (check, message.replace("<sheet_name>", self.arguments["upstream_sheet"]))
        )

        # Read upstream dataframe and identify upstream source rasters.
        upstream_df = pd.read_excel(
            self.arguments["upstream_excel"],
            sheet_name=self.arguments["upstream_sheet"],
        )
        upstream_source_rasters = [
            c
            for c in list(upstream_df)
            if c not in MANDATORY_HEADER_OVERVIEW["upstream"] and not c.startswith("_")
        ]

        # Check that at least 1 reclass rule is set to reclass = True
        check = any(upstream_df.reclass == 1)
        message = {
            True: "At least one reclass rule enabled, yeah!",
            False: "None of reclass rules are enabled with reclass == 1 ",
        }[check]
        self.checks.append((check, message))

        # Check if upstream rules are not empty
        check = ~upstream_df.loc[:, upstream_source_rasters].isnull().any().any()
        message = f"Reclass Excel {does_does_not[~check]} have missing values."
        self.checks.append((check, message))

        # Each sourceraster is mentioned in the Combined Raster VAT
        combined_vat = read_raster_vat(self.arguments["combined_raster"])

        check = (
            True
            if set(upstream_source_rasters).issubset(set(list(combined_vat)))
            else False
        )
        message = "Upstream source rasters {0} subset of Combined Raster source rasters{1}{2}".format(
            are_are_not[check],
            ": ",
            ", ".join(
                list(set(upstream_source_rasters).difference(set(list(combined_vat))))
            ),
        )
        self.checks.append((check, message))

        # Verify that valid values are provided in the upstream sheet. This means that in each source_raster column,
        # only values are employed that are present in the actual source raster. Otherwise, the user may expect to
        # use a value in reclass-rule, without realising that value doesn't exist in it.

        for upstream_source_raster in list(
            set(upstream_source_rasters).intersection(set(list(combined_vat)))
        ):
            # The set of values of the source raster as it appears in the combined raster VT
            combination_values = set(combined_vat[upstream_source_raster])

            # Gather the provided values from the column in the upstream sheet
            provided_values = set()
            for rule in upstream_df.loc[
                upstream_df[upstream_source_raster] != "n",
                upstream_source_raster,
            ].to_list():
                rule_values = parse_rule(rule)
                provided_values.update(rule_values)
            check = True if provided_values.issubset(combination_values) else False
            message = {
                True: f"Congruent values for source raster {upstream_source_raster}",
                False: f"Upstream values for {upstream_source_raster} not matched in combination raster: {', '.join([str(i) for i in provided_values.difference(combination_values)])}",
            }[check]
            self.checks.append((check, message))


class VerifyDownstreamExcelContents(VerificationProcedure):
    """
    Verify that all user input for downstream reclassificaotn is valid
    """

    def __init__(self, arguments):
        super().__init__()
        self.arguments = arguments

    def run(self):
        """
        Execute this verification procedure with the following checks:

        1. values in downstream sheet are subset of values in upstream sheet
        2. newvalue and description columns in downstream sheet are coupled 1:1
        3.
        Returns
        -------

        """

        # Check if newvalues in downstream are subset of newvalues in upstream
        downstream = pd.read_excel(
            io=self.arguments["downstream_excel"],
            sheet_name=self.arguments["downstream_sheet"],
        )
        upstream = pd.read_excel(
            io=self.arguments["upstream_excel"],
            sheet_name=self.arguments["upstream_sheet"],
        )
        check = (
            True if set(downstream.newvalue).issubset(set(upstream.newvalue)) else False
        )
        message = f"Downstream newvalues {are_are_not[check]} congruent with upstream"
        self.checks.append((check, message))

        # Check if newvalues-descriptions in the downstream sheet are coupled 1:1
        check, message = table_coupled_one_to_one(
            path=Path(self.arguments["downstream_excel"]),
            column_name1="newvalue",
            column_name2="description",
            sheet_name=self.arguments["downstream_sheet"],
        )
        self.checks.append(
            (check, message.replace("<sheet_name>", self.arguments["downstream_sheet"]))
        )


class VerifyCombinedRasterTable(VerificationProcedure):
    def __init__(self, arguments: dict):
        """

        Parameters
        ----------
        arguments:
        """
        super().__init__()
        self.arguments = arguments

    def run(self):
        """
        Execute this verification procedure with the following checks

        1. Combined raster has a valid VAT
        2. VAT has valid headers
        Returns
        -------

        """

        # Try reading the VAT.
        try:
            _ = read_raster_vat(self.arguments["combined_raster"], sample=True)
            self.checks.append((True, "Valid raster VAT"))
        except ValueError as e:
            self.checks.append((False, "Invalid raster VAT"))

        # Raster VAT has valid headers.
        if self.checks[-1][0]:
            table_headers = set(
                read_raster_vat(
                    self.arguments["combined_raster"], sample=True
                ).columns.to_list()
            )
            mandatory_headers = MANDATORY_HEADER_OVERVIEW["combined_vat"]
            if set(mandatory_headers).issubset(set(table_headers)):
                check, msg = (
                    True,
                    f"{self.arguments['combined_raster']} headers comply to expectations",
                )
            else:
                missing_headers = set(mandatory_headers).difference(set(table_headers))
                check, msg = f"{0} sheet <sheet_name> missing column(s): {1}".format(
                    self.arguments["combined_raster"], ", ".join(missing_headers)
                )
            self.checks.append((check, msg))


class MRTInputPathway(InputPathway):
    """
    Gather all user input and perfrom verifications.
    """

    def __init__(self, arguments: dict):
        super().__init__()
        self.arguments = arguments
        self.has_downstream_sheet = arguments["downstream_sheet"]
        self.procedures_batch01 = []
        self.procedures_batch02 = []

    def identify(self):
        """
        Identify the user provided input and compile list of verificationprocedures.
        Note: procedure sequence is relevant! The first batch verifies that files exist. The second batch
        assumes files exist and are readable and examine file contents.

        Parameters
        ----------
        arguments

        Returns
        -------

        """

        self.procedures_batch01.extend(
            [
                VerifyIsGeospatialRaster(
                    raster_path=Path(self.arguments["combined_raster"])
                ),
                VerifyCombinedRasterTable(self.arguments),
                VerifyExcel(
                    path=Path(self.arguments["upstream_excel"]),
                    sheet=self.arguments["upstream_sheet"],
                    table_headers=MANDATORY_HEADER_OVERVIEW["upstream"],
                ),
            ]
        )

        self.procedures_batch02.append(
            VerifyUpstreamExcelContents(arguments=self.arguments)
        )

        if self.has_downstream_sheet:
            self.procedures_batch01.append(
                VerifyExcel(
                    path=Path(self.arguments["downstream_excel"]),
                    sheet=self.arguments["downstream_sheet"],
                    table_headers=MANDATORY_HEADER_OVERVIEW["downstream"],
                )
            )
            self.procedures_batch02.append(
                VerifyDownstreamExcelContents(arguments=self.arguments)
            )

    def verify(self):
        for procedure in self.procedures_batch01:
            procedure.run()
            procedure.update_status()

        # Proceed if all checks in first batch are approved.
        if all([procedure.status for procedure in self.procedures_batch01]):
            for procedure in self.procedures_batch02:
                procedure.run()
                procedure.update_status()

        # Gather status of each procedure
        procedure_results = [
            procedure.status
            for procedure in self.procedures_batch01 + self.procedures_batch02
        ]

        # Output to log
        self.log_messages()

        # If any procedure has failed, raise RuntimeError to end flow
        if all(procedure_results):
            self.all_checks_passed = True
        else:
            raise RuntimeError("Cannot proceed due to errors in user input.")

    def log_messages(self):
        logger = get_logger()
        logger.setLevel(logging.INFO)
        for procedure in self.procedures_batch01 + self.procedures_batch02:
            for passed, message in procedure.checks:
                if passed:
                    logger.info(message)
                else:
                    logger.error(message)


@log_start_completed
def verify_input_pathway(arguments: dict):
    """
    Create, identify and verify an input pathway.

    Parameters
    ----------
    arguments

    Returns
    -------

    """
    input_pathway = MRTInputPathway(arguments=arguments)
    input_pathway.identify()
    input_pathway.verify()
    return input_pathway
