import numpy as np
import pandas as pd
import sys
import pathlib

from mnp.utils import log_start_completed

from mrt.preparation.helper_functions import parse_rule
from mrt.upstream.combined_raster import CombinedRaster


class ReclassRules:
    """ "
    This is a holder class for the DataFrame containing the Reclass-Rules from the Upstream sheet.
    """

    def __init__(self, arguments: dict):
        self.arguments = arguments
        self.rules_df = pd.read_excel(
            arguments["upstream_excel"], sheet_name=arguments["upstream_sheet"]
        )
        self.rules_df["reclass"] = np.where(self.rules_df.reclass == 1, True, False)
        self.rules_df["rule_name"] = [
            "rule{:03}".format(i + 1) for i in self.rules_df.index
        ]
        self.applicable_rules = self.rules_df.loc[self.rules_df.reclass].index
        self.source_rasters = [
            x
            for x in list(self.rules_df)
            if x.lower()
            not in [
                "newval",
                "newvalue",
                "description",
                "remark",
                "reclass",
                "newvalue",
                "new_val",
                "volgnummer",
                "rule_name",
            ]
            and not x.startswith("_")
        ]

    def parse_rules_to_queries(self):
        """
        Create query-string for each reclass rule.
        Returns
        -------

        Examples
        -------
        Reclass rules look like:
        newvalue|description|source_rasterA|source_rasterB|source_rasterC
        1       |bos        | 100          | n            | 12
        2       |weiland    | 200,210,220  | 40           | n
        3       |stad       | n            | -40, 50      | 13

        This function adds a new column as follows:
        newvalue|...|mrt_query: str
        1       |   |`source_rasterA` in [100] AND `source_rasterC` IN [12]
        2       |   |`source_rasterA` in [200, 210, 220] AND `source_rasterB` in [40]
        3       |   |`source_rasterB` not in [40, 50] AND `source_rasterC` in [13]
        """

        # Add new temporary column for each source raster, containing a string representation of the source_raster instructions
        for source_raster in self.source_rasters:
            self.rules_df[f"src_query_{source_raster}"] = self.rules_df.loc[
                :, source_raster
            ].apply(parse_rule, category=source_raster)

        # Join the piece-queries of each source raster from the temporary columns together into a single query.
        self.rules_df["mrt_query"] = self.rules_df.apply(
            lambda row: " and ".join(
                [
                    str(i)
                    for i in row.loc[
                        row.index.str.startswith("src_query_") & row.notna()
                    ]
                ]
            ),
            axis=1,
        )
        # Drop temporary columns
        self.rules_df.drop(
            labels=[c for c in list(self.rules_df) if c.startswith("src_query")],
            axis=1,
            inplace=True,
        )
        # Replace queries with zero length, meaning that all source rasters are set to 'n', to generic catch none query
        self.rules_df.loc[self.rules_df.mrt_query.str.len() == 0, "mrt_query"] = (
            "index != index"
        )

    def apply_rules_to_combined_raster(self, combined_raster: CombinedRaster):
        """
        Apply reclass rules against combined raster VAT to determine first applicable rule to each combination value.
        Also determine gross and net-area of each reclass rule

        Parameters
        ----------
        combined_raster
        """

        gross_areas = []
        net_areas = []

        # Assign all combined-raster values to rule999-NoData as a starting point.
        combined_raster.vat["firstrule"] = "rule999"
        combined_raster.vat["newvalue"] = self.arguments["nodata"]
        rows_not_assigned = combined_raster.vat.index

        # Loop over the rules, determine which combined value(s) comply to the rule.
        for rule in self.rules_df.itertuples():
            # Identify ALL rows in combi_raster.rat that comply to the reclass rule
            gross_vat_match = combined_raster.vat.query(rule.mrt_query).index

            # Keep only those rows that have not yet been assigned
            net_vat_match = gross_vat_match.intersection(rows_not_assigned)

            # Calculate gross match in hectare
            gross_areas.append(
                np.multiply(
                    combined_raster.vat.loc[gross_vat_match, "Count"].sum(),
                    combined_raster.pixel_area_ha,
                )
            )

            # Assign this rule as the applicable rule only if the rule is set to reclass=True
            if rule.reclass:
                combined_raster.vat.loc[net_vat_match, "firstrule"] = rule.rule_name
                combined_raster.vat.loc[net_vat_match, "newvalue"] = rule.newvalue

                # Calculate net match
                net_areas.append(
                    np.multiply(
                        combined_raster.vat.loc[net_vat_match, "Count"].sum(),
                        combined_raster.pixel_area_ha,
                    )
                )

                # Remove net_rat_match rows from unassigned rows
                rows_not_assigned = rows_not_assigned.difference(net_vat_match)
            else:
                net_areas.append(0)

        self.rules_df["gross_match_ha"] = gross_areas
        self.rules_df["net_match_ha"] = net_areas
        self.rules_df["difference_ha"] = self.rules_df.gross_match_ha.subtract(
            self.rules_df.net_match_ha
        )


@log_start_completed
def get_reclass_rules(arguments: dict, combined_raster: CombinedRaster):
    """
    Create and return a ReclassRules object. This contains a DataFrame where each row is a reclass-rule.


    Parameters
    ----------
    arguments: dictionary with user-specified arguments
    combined_raster: combined_raster object, containing the combined raster itself plus Value Attribute Table

    Returns
    -------

    """

    reclass_rules = ReclassRules(arguments=arguments)

    # Parse each reclass rule to a string query
    reclass_rules.parse_rules_to_queries()

    # Evaluate each query against the combined raster VAT. Calculate gross- and net areas.
    reclass_rules.apply_rules_to_combined_raster(combined_raster=combined_raster)
    return reclass_rules
