import os
from rasterio.windows import Window
import numpy as np
import rasterio as rio

from mrt.preparation.helper_functions import read_raster_vat

from mnp.utils import log_start_completed


class SourceRaster:
    """
    Class for holding all information of a source raster incorporated into a CombinedRaster, as derived from the
    CombinedRaster RAT
    """

    def __init__(self, name, vals):
        self.name = name
        self.vals = vals


class CombinedRaster:
    """
    Class holding all information related to an integer raster + VAT resulting from arcpy.combine or the COMBINER Tool
    See: https://git.wur.nl/roelo008/combiner
    """

    def __init__(self, arguments: dict):
        self.arguments = arguments
        self.raster_source = arguments["combined_raster"]
        self.rast_dir = os.path.dirname(arguments["combined_raster"])
        self.basename, self.ext = os.path.splitext(arguments["combined_raster"])
        self.raster = rio.open(arguments["combined_raster"])
        self.vat = read_raster_vat(arguments["combined_raster"])
        self.pxl_area_m2 = np.square(
            self.raster.res[0]
        )  # assumes raster CRS is projected in meters!
        self.pixel_area_ha = np.divide(self.pxl_area_m2, 10000)
        self.source_rasters = [
            SourceRaster(name=c, vals=set(self.vat.loc[:, c]))
            for c in list(self.vat)
            if c.lower() not in ["value", "count", "geometry"]
        ]
        self.bounds = self.raster.bounds
        self.window = Window(
            0, 0, width=self.raster.shape[1], height=self.raster.shape[0]
        )


@log_start_completed
def get_combined_raster(arguments: dict):
    """
    Instantiate and return CombinedRaster object
    Parameters: dict with user defined arguments.
    ----------
    arguments

    Returns
    -------

    """
    return CombinedRaster(arguments=arguments)
