import datetime
import os

DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"


def metadata_dict(arguments: dict) -> dict:
    """
    Generates standardized metadata about this MRT run.

    Parameters
    ----------
    arguments: dict with user-specified arguments for MRT run

    Returns
    -------
    metadata dictionary

    """
    metadata_dict = {
        "upstream_excel": arguments["upstream_excel"],
        "upstream_sheet": arguments["upstream_sheet"],
        "upstream_excel_created": datetime.datetime.fromtimestamp(
            os.path.getctime(arguments["upstream_excel"])
        ).strftime(DATETIME_FORMAT),
        "upstream_excel_last_modified": datetime.datetime.fromtimestamp(
            os.path.getmtime(arguments["upstream_excel"])
        ).strftime(DATETIME_FORMAT),
        "combined_raster": arguments["combined_raster"],
        "mrt_timestamp": arguments["mrt_timestamp"].strftime(DATETIME_FORMAT),
        "created_by": os.environ.get("username"),
    }

    if arguments["downstream_sheet"]:
        metadata_dict["downstream_excel"] = arguments["downstream_excel"]
        metadata_dict["downstream_sheet"] = arguments["downstream_sheet"]
        if arguments["downstream_excel"] != arguments["upstream_excel"]:
            metadata_dict["downstream_excel_created"] = datetime.datetime.fromtimestamp(
                os.path.getctime(arguments["downstream_excel"])
            ).strftime(DATETIME_FORMAT)
        metadata_dict["dowstream_excel_last_modified"] = (
            datetime.datetime.fromtimestamp(
                os.path.getmtime(arguments["downstream_excel"])
            ).strftime(DATETIME_FORMAT)
        )

    return metadata_dict
