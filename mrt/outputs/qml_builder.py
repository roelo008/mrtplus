import xml.etree.ElementTree as ET
import os
import sys
import pathlib

import pandas as pd

from mrt.outputs.sidecars import QML, bdb_legenda_rogier, rand_web_color_hex


def build_qml(d: dict, dest):
    """

    Parameters
    ----------
    d: dictioanary as {<pixel value>: <category>}

    Returns
    -------

    """

    df = pd.DataFrame.from_dict(d, orient="index", columns=["category"]).sort_index()

    template = str(
        os.path.join(
            str(pathlib.Path(__file__).parents[2]),
            "resources/bdb_huidig_qgis_style_file.qml",
        )
    )

    tree = ET.parse(template)
    root = tree.getroot()
    target = root[2][0][2]
    assert target.tag == "colorPalette"
    for x in target.findall("paletteEntry"):
        target.remove(x)

    for row in df.itertuples():
        try:
            d = bdb_legenda_rogier[row.category]
            col, alpha = (d['color'], d['alpha'])
            print(f'  found {row.category}')
        except KeyError:
            print(f'random color for {row.category}')
            col, alpha = rand_web_color_hex(), "255"
        ET.SubElement(
            target,
            "paletteEntry",
            attrib={
                "color": col,
                "value": str(row.Index),
                "label": "{0}: {1}".format(row.Index, row.category),
                "alpha": alpha,
            },
        )
    tree.write(f"{dest}.qml")


if __name__ == "__main__":
    import argparse

    argument_parser = argparse.ArgumentParser(description="build QML based on CSV file")
    argument_parser.add_argument("csv", help="path to CSV file", type=str)
    argument_parser.add_argument(
        "category_column", help="name of column with category descriptions", type=str
    )
    argument_parser.add_argument(
        "value_column", help="name of column with  pixel values", type=str
    )
    argument_parser.add_argument(
        "destination", help="path and filename of output QML file", type=str
    )
    args = argument_parser.parse_args()

    d = pd.read_csv(args.csv, index_col=args.value_column)[
        args.category_column
    ].to_dict()
    build_qml(d, args.destination)
