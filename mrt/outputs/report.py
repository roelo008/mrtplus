import os
import pathlib
import sys
from typing import List

import numpy as np
import pandas as pd

from mrt.downstream.reclassifiers import ReClassifier
from mrt.outputs.metadata import metadata_dict

from mnp.utils import log_start_completed, get_logger

EXCEL_ROW_MAXIMUM = 1048576


class Report:
    def __init__(self, arguments):
        self.arguments = arguments
        self.output_directory = arguments["out_dir"]
        self.output_name = f'{arguments["basename"]}_report.xlsx'
        self.models = []
        self.metadata = metadata_dict(arguments)

    def write_report(
        self, reclass_rules, combined_raster, reclassifiers: List[ReClassifier]
    ):
        """
        Write report on this MRT run to file.
        Parameters
        ----------
        reclass_rules
        combined_raster
        reclassifiers

        Returns
        -------

        """

        logger = get_logger()

        with pd.ExcelWriter(
            os.path.join(self.output_directory, self.output_name), mode="w"
        ) as writer:
            # Write metadata sheet
            pd.DataFrame.from_dict(
                self.metadata, orient="index", columns=["_"]
            ).to_excel(writer, sheet_name="metadata", index=True, freeze_panes=(1, 1))

            # Write reclass rules to sheet, add line with not-classified info
            not_classified_gross = np.multiply(
                np.prod(combined_raster.raster.shape, dtype=np.uint64),
                combined_raster.pixel_area_ha,
            )
            not_classified_net = np.subtract(
                not_classified_gross, reclass_rules.rules_df.net_match_ha.sum()
            )

            d = dict.fromkeys(
                list(reclass_rules.rules_df),
            )
            d.update(
                newvalue=self.arguments["nodata"],
                description="no data",
                remark="auto generated rule",
                volgnummer=reclass_rules.rules_df.shape[0] + 1,
                reclass=True,
                rule_name="rule999",
                mrt_query="index == index",
                gross_match_ha=not_classified_gross,
                net_match_ha=not_classified_net,
                difference_ha=np.subtract(not_classified_gross, not_classified_net),
            )
            for source_raster in combined_raster.source_rasters:
                d[source_raster.name] = "n"

            pd.concat(
                [
                    reclass_rules.rules_df,
                    pd.DataFrame(d, index=[reclass_rules.rules_df.index.stop + 1]),
                ]
            ).to_excel(
                writer, sheet_name="reclass_rules", index=False, freeze_panes=(1, 2)
            )

            # Write combined raster VAT to sheet if #rows < maximum rows in Excel: 1.048.576
            if combined_raster.vat.shape[0] < EXCEL_ROW_MAXIMUM:
                logger.info("writing combined raster VAT")
                combined_raster.vat.to_excel(
                    writer, sheet_name="reclassed", index=False, freeze_panes=(1, 1)
                )

            # Write downstream sheet
            if self.arguments["downstream_sheet"]:
                df = pd.read_excel(
                    self.arguments["downstream_excel"],
                    sheet_name=self.arguments["downstream_sheet"],
                )
                df.loc[
                    df.newvalue.isin(
                        reclass_rules.rules_df.loc[
                            reclass_rules.applicable_rules, "newvalue"
                        ]
                    )
                ].to_excel(
                    writer, sheet_name="downstream", index=False, freeze_panes=(1, 1)
                )

            # Write a sheet for each reclassifier, with area statistics
            for reclassifier in reclassifiers:
                logger.info(f"writing model area df: {reclassifier.name}")
                df = reclassifier.return_area_df()
                df.to_excel(
                    writer,
                    sheet_name=f"{reclassifier.name}_area_stats",
                    index=False,
                    freeze_panes=(1, 1),
                )


@log_start_completed
def write_report(
    arguments: dict, reclass_rules, combined_raster, reclassifiers: List[ReClassifier]
):
    report = Report(arguments=arguments)
    report.write_report(
        reclass_rules=reclass_rules,
        combined_raster=combined_raster,
        reclassifiers=reclassifiers,
    )
