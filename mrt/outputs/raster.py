import random
import os
from typing import List, Tuple

import affine
import rasterio as rio
import numpy as np
from rasterio import MemoryFile
from rasterio.coords import BoundingBox
from rasterio.enums import Resampling
from rasterio.windows import Window

from mrt.downstream.reclassifiers import ReClassifier
from mrt.upstream.combined_raster import CombinedRaster
from mrt.outputs.metadata import metadata_dict

from mnp.utils import log_start_completed, get_logger



def aggregate_numpy_array(
    arr: np.ndarray,
    profile: dict,
    target_width: int,
    target_height: int,
    resample_method: str,
) -> np.ndarray:
    """
    Aggregate a numpy array, ie reduce its height/width, using a gdal resampling method.

    Parameters
    ----------
    arr: target array,
    target_height: height of output array
    target_width: width of output array
    resample_method: one of
        average
        bilinear
        cubic
        cubic_spline
        gauss
        lanczos
        max
        med
        min
        mode
        nearest
        q1
        q3
        rms
        sum
    Returns
    -------
    Numpy array with new dimensions.
    """

    with MemoryFile() as memfile:
        with memfile.open(**profile) as dataset:
            dataset.write(arr, 1)

        with memfile.open() as dataset:
            return dataset.read(
                1,
                out_shape=(1, target_height, target_width),
                resampling=getattr(Resampling, resample_method),
            )


class OutputRaster:
    """
    Class for a geospatial raster produced by the MRT
    """

    def __init__(self, arguments: dict):
        """

        Parameters
        ----------
        arguments: MRT.py main arguments dictionary
        """
        self.arguments = arguments
        self.destination = os.path.join(
            arguments["out_dir"],
            f'{arguments["basename"]}_{arguments["target"]}.{arguments["raster_extension"]}',
        )
        self.metadata = metadata_dict(arguments)
        self.target_profile = {}
        self.write_windows: List[Window] = []
        self.target_bounds: rio.coords.BoundingBox = BoundingBox(0, 0, 0, 0)
        self.transform: affine.Affine = affine.Affine(0, 0, 0, 0, 0, 0)
        self.aggregation_factor: int = 0

    def build_target_profile(
        self, combined_raster: CombinedRaster, target_reclassifier: ReClassifier
    ):
        """
        Gather all specifications of the output raster.
        Returns
        -------

        """

        # Build output profile based on user specifications
        self.target_bounds = (
            BoundingBox(*self.arguments["xtnt"])
            if self.arguments["xtnt"]
            else combined_raster.raster.bounds
        )

        target_resolution = (
            self.arguments["tr"]
            if self.arguments["tr"]
            else combined_raster.raster.res[0]
        )

        self.transform = affine.Affine(
            a=target_resolution,
            b=0,
            c=self.target_bounds.left,
            d=0,
            e=target_resolution * -1,
            f=self.target_bounds.top,
        )

        self.aggregation_factor = int(target_resolution / combined_raster.raster.res[0])

        # Build geospatial raster profile of the output raster
        self.target_profile = rio.default_gtiff_profile
        self.target_profile.update(
            driver=self.arguments["of"],
            dtype=rio.dtypes.get_minimum_dtype(
                list(target_reclassifier.combined_value_to_outvalue.values())
                + [self.arguments["nodata"]]
            ),
            nodata=self.arguments["nodata"],
            width=int(
                (self.target_bounds.right - self.target_bounds.left) / target_resolution
            ),
            height=int(
                (self.target_bounds.top - self.target_bounds.bottom) / target_resolution
            ),
            transform=self.transform,
            count=1,
            crs=combined_raster.raster.crs,
        )

    def build_read_write_windows(
        self, combined_raster: CombinedRaster
    ) -> Tuple[List[Window], List[Window]]:
        """
        Determine intersection between target-bounds and combined-raster-bounds. Compile two lists of Windows:

        READ windows are referenced towards the combined_raster (using combined_raster.transform).
          height = aggregation factor.
          width = width of intersected window, in pixels in combined_raster_resolution
          row, col offset is position of intersected window relative to combined raster

        WRITE windows are referenced towards the target raster.
          height = 1 pixel of target raster resolution
          width = width of intersected window, in pixels in target_resolution
          row, col offset is position of intersected window relative to target raster.

        If target raster extent is fully within combined raster extent, intersected window equals target window.

        Parameters
        ----------
        combined_raster

        Returns
        -------
        ([read_window01, read_window02 ...], [write_window01, write_window02 ...])

        """

        windows = []

        # Use Affine transformation of first the combined raster, then the target raster
        for rw, transform in zip(
            ["read", "write"], [combined_raster.raster.transform, self.transform]
        ):
            target_window = rio.windows.from_bounds(
                left=self.target_bounds.left,
                right=self.target_bounds.right,
                bottom=self.target_bounds.bottom,
                top=self.target_bounds.top,
                transform=transform,
            )
            combined_window = rio.windows.from_bounds(
                left=combined_raster.bounds.left,
                bottom=combined_raster.bounds.bottom,
                right=combined_raster.bounds.right,
                top=combined_raster.bounds.top,
                transform=transform,
            )
            intersected_window = rio.windows.intersection(
                target_window, combined_window
            )

            windows.append(
                [
                    Window(
                        col_off=intersected_window.col_off,
                        row_off=i,
                        height={"read": self.aggregation_factor, "write": 1}[rw],
                        width=intersected_window.width,
                    )
                    for i in range(
                        int(intersected_window.row_off),
                        int(intersected_window.row_off + intersected_window.height),
                        {"read": self.aggregation_factor, "write": 1}[rw],
                    )
                ]
            )

        assert len(windows[0]) == len(
            windows[1]
        ), f"Error {len(windows[0])} read windows and {len(windows[1])} write!"
        return windows[0], windows[1]

    def reclass_and_write_mono(
        self,
        target_reclassifier: ReClassifier,
        combined_raster: CombinedRaster,
        testing: bool = False,
    ):
        """
        Write the target geospatial raster to file.
        Parameters
        ----------
        target_reclassifier
        combined_raster
        testing: bool: write 0.2% of output lines
        Returns
        -------

        """

        logger = get_logger()

        read_windows, write_windows = self.build_read_write_windows(
            combined_raster=combined_raster
        )

        if testing:
            logger.info("**SAMPLE MODE**")
            sample_indices = random.sample(
                range(len(read_windows)), len(read_windows) // 500
            )
            read_windows = [read_windows[i] for i in sample_indices]
            write_windows = [write_windows[i] for i in sample_indices]

        with rio.open(self.destination, "w", **self.target_profile) as dest:
            dest.update_tags(**metadata_dict(self.arguments))
            dest.update_tags(
                **{
                    str(j): str(i)
                    for i, j in target_reclassifier.category_to_outvalue.items()
                }
            )

            for i, rw in enumerate(zip(read_windows, write_windows), start=1):
                read_window, write_window = rw

                if i % 100 == 0:
                    logger.info(f"reclassing raster window {i}/{len(write_windows)}")

                # Read portion from the combined raster
                combined_array: np.array = combined_raster.raster.read(
                    1, window=read_window
                )

                # Reclassify using the ReClassifier object
                reclassed_array = np.vectorize(
                    target_reclassifier.combined_value_to_outvalue.__getitem__
                )(combined_array)

                # Aggregate if needed
                if self.aggregation_factor > 1:
                    profile = rio.default_gtiff_profile.copy()
                    profile.update(
                        width=reclassed_array.shape[1],
                        height=reclassed_array.shape[0],
                        dtype=rio.dtypes.get_minimum_dtype(reclassed_array),
                    )

                    reclassed_array = aggregate_numpy_array(
                        reclassed_array,
                        profile=profile,
                        target_width=int(write_window.width),
                        target_height=int(write_window.height),
                        resample_method=self.arguments["resample"],
                    )

                # Write to file
                dest.write(reclassed_array, 1, window=write_window)

    def reclass_and_write_parallel(
        self,
        target_reclassifier: ReClassifier,
    ):
        raise NotImplementedError("Parallel processing not implemented")


@log_start_completed
def write_raster(
    arguments: dict, reclassifiers: List[ReClassifier], combined_raster: CombinedRaster
) -> None:
    """
    Prefect Task. Write a geospatial raster to file, according to instructions in arguments.
    Parameters
    ----------
    arguments
    reclassifiers
    combined_raster

    Returns
    -------

    """

    target_reclassifier = [x for x in reclassifiers if x.name == arguments["target"]][0]
    target_reclassifier.build_combined_value_to_outvalue(
        combined_raster=combined_raster, output_nodata=arguments["nodata"]
    )
    raster = OutputRaster(arguments=arguments)
    raster.build_target_profile(
        combined_raster=combined_raster, target_reclassifier=target_reclassifier
    )
    raster.reclass_and_write_mono(
        target_reclassifier=target_reclassifier,
        combined_raster=combined_raster,
        testing=arguments["testing"],
    )
