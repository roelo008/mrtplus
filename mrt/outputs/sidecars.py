import os
import logging
import random
from importlib.resources import files as import_files
import xml.etree.ElementTree as ET
from abc import abstractmethod
from multiprocessing.util import get_logger
from typing import List

import geopandas as gp
import numpy as np
import pandas as pd

from mnp.utils import log_start_completed, get_logger
from model_framework.input_verification_functions import default_land_type_colors, rand_web_color_hex

from mrt.downstream.reclassifiers import ReClassifier


class SideCar:
    def __init__(self, arguments: dict, target_reclassifier: ReClassifier):
        """
        Parent class for files as sidecar alongside Geospatial raster.
        Parameters
        ----------
        arguments
        target_reclassifier
        """
        self.arguments = arguments
        self.reclassifier = target_reclassifier
        self.destination = os.path.join(
            arguments["out_dir"], f'{arguments["basename"]}_{arguments["target"]}'
        )

        categories_with_area_ = target_reclassifier.return_area_df().query('area_ha > 0').category.to_list()

        self.outvalue_to_category = {
            j: i for i, j in self.reclassifier.category_to_outvalue.items() if i in categories_with_area_
        }
        self.df = pd.DataFrame.from_dict(
            self.outvalue_to_category, orient="index", columns=["category"]
        ).sort_index()


    @abstractmethod
    def build(self) -> None:
        """
        Build sidecar contents and save to file
        Returns
        -------

        """
        pass


class QML(SideCar):
    """
    QGIS QML style file sidecar
    """

    def __init__(self, arguments: dict, target_reclassifier: ReClassifier):
        super().__init__(arguments, target_reclassifier)
        self.template_path = import_files("mrt.resources").joinpath("bdb_huidig_qgis_style_file.qml")
        self.extension = "qml"

    def build(self):
        tree = ET.parse(self.template_path)
        root = tree.getroot()
        target = root[2][0][2]
        assert target.tag == "colorPalette"
        for x in target.findall("paletteEntry"):
            target.remove(x)

        logger = get_logger()
        logger.setLevel(logging.INFO)

        for row in self.df.itertuples():
            try:
                col = default_land_type_colors[row.category]
                alpha = "255"
            except KeyError:
                logger.info(f'random color for {row.category}')
                col, alpha = rand_web_color_hex(), "255"
            ET.SubElement(
                target,
                "paletteEntry",
                attrib={
                    "color": col,
                    "value": str(row.Index),
                    "label": "{0}: {1}".format(row.Index, row.category),
                    "alpha": alpha,
                },
            )
        tree.write(f"{self.destination}.{self.extension}")


class VAT(SideCar):
    """
    Value Attribute Table Sidecar
    """

    def __init__(self, arguments: dict, target_reclassifier: ReClassifier):
        super().__init__(arguments, target_reclassifier)
        self.extension = f'{self.arguments["raster_extension"]}.vat.dbf'

    def build(self):
        gdf = gp.GeoDataFrame(
            data=self.df.assign(value=self.df.index, geometry=None)
        ).set_geometry("geometry")
        gdf.to_file(f"{self.destination}.shp")

        # Remove redundant Shapefile components
        for f in os.listdir(os.path.dirname(self.destination)):
            if f.endswith(
                tuple(
                    [
                        f"{os.path.splitext(os.path.basename(self.destination))[0]}{ext}"
                        for ext in [
                            ".shp",
                            ".shx",
                            ".prj",
                            ".sbn",
                            ".sbx",
                            ".cpg",
                            "shp.xml",
                        ]
                    ]
                )
            ):
                os.remove(os.path.join(os.path.dirname(self.destination), f))

        # Rename DBF file
        try:
            os.rename(
                f"{self.destination}.dbf",
                f'{self.destination}.{self.arguments["raster_extension"]}.vat.dbf',
            )
        except FileExistsError:
            logger = get_logger()
            logger.info(
                f'{self.destination}.{self.arguments["raster_extension"]}.vat.dbf already exists?!'
            )


class MNPLegend(SideCar):
    """
    Write CSV file that complies to land-type map legend in MNP
    See: https://opengis.wur.nl/MNP/contents/data_specificaties.html#land-type-raster-legenda-tabel
    """

    def __init__(self, arguments: dict, target_reclassifier: ReClassifier):
        super().__init__(arguments, target_reclassifier)
        self.table_columns = ["land_type_code", "land_type_name", "land_type_id"]
        self.extension = "csv"

    def build(self):
        # Assumes land  type descriptions are formatted as [A-Z][0-9]{2}.[0-9]{2}.[0-9]{2} <LAND TYPE DESCRIPTION>
        # Eg: N16.03.00 Droog bos met Productie
        self.df[self.table_columns[2]] = self.df.index
        self.df[self.table_columns[0]] = np.where(
            self.df.category.str.split(" ", n=1, expand=True)[0].str.len() == 6,
            self.df.category.str.split(" ", n=1, expand=True)[0] + ".00",
            self.df.category.str.split(" ", n=1, expand=True)[0],
        )
        self.df.rename(columns={"category": self.table_columns[1]}, inplace=True)

        self.df.query(f"{self.table_columns[0]} != 'geen'").sort_values(
            by=self.table_columns[2]
        ).to_csv(
            f"{self.destination}.{self.extension}", index=False, header=True, sep=","
        )


@log_start_completed
def build_side_cars(
    arguments: dict,
    reclassifiers: List[ReClassifier],
    qml: bool = True,
    vat: bool = True,
) -> None:
    """
    Build mandatory sidecars and optional MNP attribute table.
    Parameters
    ----------
    reclassifiers
    arguments
    models
    qml
    vat

    Returns
    -------

    """
    reclassifier_target = [
        reclassifier
        for reclassifier in reclassifiers
        if reclassifier.name == arguments["target"]
    ][0]

    if qml:
        qml_sidecar = QML(arguments, reclassifier_target)
        qml_sidecar.build()

    if vat:
        vat_sidecar = VAT(arguments, reclassifier_target)
        vat_sidecar.build()

    if arguments["mnp_legend"]:
        mnp_legend = MNPLegend(arguments, reclassifier_target)
        mnp_legend.build()
