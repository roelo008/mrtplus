import rasterio as rio
from typing import List
import numpy as np
import pandas as pd

from mnp.utils import get_logger, log_start_completed

from mrt.upstream.combined_raster import CombinedRaster
from mrt.upstream.reclass_rules import ReclassRules


class ReClassifier:
    """

    A ReClassifier object reclassifies the values of the Combined Raster to user-specified values and categories.
    A Reclassifier Object is keyed towards either the newvalue or the firstrule, meaning that reclassification of each combined
    value follows either its corresponding newvalue or firstrule.

    A ReClassifier object holds 4 dictionaries.

    1. key-to-category.      This maps the key to the output category.
    2. category-to-outvalue. This maps each output category to its corresponding integer output value. This can be
                             user-specified, or enumeration of the catogories
    3. key-to-outvalue       This maps the key to the outputvalue. This dict is a derivative of the previous two.
    4. combined-value-to-outvalue: This maps all values in the combined raster to the outputvalue. This dict is not
                                   generated until required for writing an output raster.

    Examples
    -------

    ==Upstream sheet==
    newvalue|description    |source_raster01|source_raster02|rulename
    1       |eikenbeukenbos |12             |n              |rule01
    1       |eikenbeukenbos |n              |400            |rule02
    2       |populierbos    |13,14,15       |n              |rule03
    2       |populierbos    |n              |500            |rule04
    3       |naaldbos       |16, 17         |600            |rule05
    4       |weiland        |20, 22, 23,    |n              |rule06
    5       |mais           |25, 26         |200            |rule07

    Example 01: downstream model based on newvalue

    ==Downstream sheet==
    newvalue|description    |downstream_model01
    1       |eikenbeukenbos | bos
    2       |populierbos    | bos
    3       |naaldbos       | bos
    4       |weiland        | gras
    5       |mais           | akker

    ==downstream_model01 sheet==
    Description|Value
    bos        |100
    gras       |101
    akker      |102

    downstream_model01.key_to_category = {
      1: bos,
      2: bos,
      3: bos,
      4: gras,
      5: akker,
      }

    downstream_model01.category_to_outvalue = {
      bos: 100,
      gras: 101,
      akker: 102,
      }

    downstream_model01.key_to_outvalue = {
        1: 100,
        2: 100,
        3: 100,
        4: 101,
        5: 102,
        }

    Example 02: downstream model based on firstrule

    ==Upstream sheet==
    newvalue|description    |source_raster01|source_raster02|_downstream_model02|rule_name
    1       |eikenbeukenbos |12             |n              |A                  |rule01
    1       |eikenbeukenbos |n              |400            |A                  |rule02
    2       |populierbos    |13,14,15       |n              |B                  |rule03
    2       |populierbos    |n              |500            |B                  |rule04
    3       |naaldbos       |16, 17         |600            |C                  |rule05

    ==downstream_model02 sheet== (optional)
    Description|Value
    A          |1
    B          |2
    C          |3

    downstream_model02.key_to_category = {
      rule01: A,
      rule02: A,
      rule03: B,
      rule04: B,
      rule05: C,
      }

    downstream_model02.category_to_outvalue = {
      A: 1,
      B: 2,
      C: 3,
      }

    downstream_model02.key_to_outvalue = {
      rule01: 1,
      rule02: 1,
      rule03: 2,
      rule04: 2,
      rule05: 3,
      }
    """

    def __init__(self, name: str, key: str):
        self.name = name  # name of the model, ie column in downstream sheet
        self.source = ""
        self.key = key
        self.key_to_category = {}
        self.category_to_outvalue = {}
        self.key_to_outvalue = {}
        self.combined_value_to_outvalue = {}
        self.area_df = None

    def build_combined_value_to_outvalue(
        self, combined_raster: CombinedRaster, output_nodata: int
    ):
        """
        Build dictionary that maps all Combined Value raster values to output values. Also consider user-specified
        NoData and NoData of the Combined Raster

        Parameters
        ----------
        output_nodata
        combined_raster

        Returns
        -------

        """

        # Mapping all combined value to output value.
        working_df = combined_raster.vat.set_index("Value").loc[:, self.key]
        working_df["outvalue"] = (
            working_df.map(self.key_to_outvalue)
            .fillna(output_nodata)
            .astype(
                rio.dtypes.get_minimum_dtype(
                    list(self.key_to_outvalue.values()) + [output_nodata]
                )
            )
        )
        self.combined_value_to_outvalue = working_df.outvalue.to_dict()

        # Add the NoData value of the Combined Raster to the reclass dict.
        if combined_raster.raster.nodata:
            self.combined_value_to_outvalue[combined_raster.raster.nodata] = (
                output_nodata
            )

    def calculate_areas(self, combined_raster: CombinedRaster):
        """

        Build dataframe with area in ha for each output category, based on combined_raster VAT.

        Examples
        ----------
        =downstream_model01.area_df
        category   |outvalue|area_ha|
        bos        |100     |xxx
        gras       |101     |xxx
        akker      |102     |xxx

        Parameters
        ----------
        combined_raster: combined raster object, including raster VAT.

        Returns
        -------

        """

        # Add model output category to each row in the combined_raster VAT
        # Pivot on category, calculate areas from 'count' column
        df1 = pd.pivot_table(
            combined_raster.vat.loc[
                combined_raster.vat.firstrule != "rule999",
                ["Count", "firstrule", "newvalue"],
            ].assign(
                category=combined_raster.vat.loc[:, self.key].map(self.key_to_category)
            ),
            index="category",
            values="Count",
            aggfunc=lambda s: np.multiply(np.sum(s), combined_raster.pixel_area_ha),
        ).rename(columns={"Count": "area_ha"})

        # Merge with second dataframe
        df1 = pd.merge(
            df1,
            pd.DataFrame.from_dict(
                self.category_to_outvalue, orient="index", columns=["outvalue"]
            ),
            left_index=True,
            right_index=True,
            how="right",
        ).fillna(0)
        df1["category"] = df1.index
        setattr(self, "area_df", df1.sort_values(by="outvalue"))

    def return_area_df(self):
        """

        Returns: area dataframe
        -------

        """

        if isinstance(self.area_df, pd.DataFrame):
            return self.area_df.loc[:, ["category", "outvalue", "area_ha"]]
        else:
            raise ValueError(f"Area Dataframe for {self.name} not instantiated.")


class DefaultReClassifierNewvalue(ReClassifier):
    """
    Special implementation of a ReClassifier. This is a ReClassifier that maps the newvalues from the upstream-sheet.
    This is a default ReClassifier, meaning it is always present.
    """

    def __init__(self):
        super().__init__(
            name="newvalue",
            key="newvalue",
        )

    def build(self, reclass_rules: ReclassRules):
        """
        Build key_to_category, category_to_outvalue and key_to_outvalue dictionaries
        Parameters
        ----------
        reclass_rules

        Returns
        -------

        """

        self.key_to_category = (
            reclass_rules.rules_df.loc[reclass_rules.applicable_rules]
            .set_index("newvalue")
            .description.to_dict()
        )
        self.category_to_outvalue = (
            reclass_rules.rules_df.loc[reclass_rules.applicable_rules]
            .set_index("description")
            .newvalue.to_dict()
        )
        self.key_to_outvalue = dict(
            zip(
                reclass_rules.rules_df.loc[reclass_rules.applicable_rules].newvalue,
                reclass_rules.rules_df.loc[reclass_rules.applicable_rules].newvalue,
            )
        )


class DefaultReClassifierFirstrule(ReClassifier):
    """
    Default Reclassifier. Maps FirstRule to output values
    """

    def __init__(self):
        super().__init__(name="firstrule", key="firstrule")

    def build(self, reclass_rules: ReclassRules):
        """
        Build key_to_category, category_to_outvalue and key_to_outvalue dictionaries
        Parameters
        ----------
        reclass_rules

        Returns
        -------

        """
        self.key_to_category = dict(
            zip(
                reclass_rules.rules_df.loc[reclass_rules.applicable_rules, "rule_name"],
                reclass_rules.rules_df.loc[reclass_rules.applicable_rules, "rule_name"],
            )
        )
        self.category_to_outvalue = {
            j: i
            for i, j in enumerate(
                pd.unique(
                    reclass_rules.rules_df.loc[reclass_rules.applicable_rules]
                    .sort_values(by="rule_name")
                    .rule_name
                ),
                start=1,
            )
        }
        self.key_to_outvalue = dict(
            zip(
                [k for k, _ in self.key_to_category.items()],
                [self.category_to_outvalue[v] for _, v in self.key_to_category.items()],
            )
        )


class DownstreamReClassifier(ReClassifier):
    """
    Class for holding user-specified, non-default, downstream ReClassifiers
    """

    def __init__(self, name: str, key: str, definition_df: pd.DataFrame):
        """

        Parameters
        ----------
        name
        key
        definition_df: Dataframe with the defintion of the ReClassifier. If key=firstrule --> reclass_rules If key=newvalue --> downstream sheet

        """
        super().__init__(name=name, key=key)
        self.definition_df = definition_df

    def build_key_to_category(
        self,
    ):
        """
        Build key_to_category dictionary
        Returns
        -------

        """

        index_name = {"newvalue": "newvalue", "firstrule": "rule_name"}[self.key]

        self.key_to_category = (
            self.definition_df.dropna(subset=[self.name])
            .set_index(index_name)
            .loc[:, self.name]
            .to_dict()
        )

    def build_category_to_outvalue(self, excel_source):
        """
        Build category_to_outvalue dictionary
        Returns
        -------

        """

        # Build mapping from output category to output value. Try looking for dedicated sheet first
        try:
            # TODO: verify column names [Description, Value]. Raise error if sheet exists but column names are wrong

            df = pd.read_excel(excel_source, sheet_name=self.name).set_index(
                "Description"
            )
            self.category_to_outvalue = df.Value.to_dict()
            self.source = "user_specified"

        except ValueError:
            # If no dedicated sheet found, enumerate the output categories
            self.category_to_outvalue = {
                j: i
                for i, j in enumerate(
                    set([v for _, v in self.key_to_category.items()]), start=1
                )
            }
            self.source = "enumerated"

        logger = get_logger()
        logger.info(f"Output values for {self.name} are {self.source}.")

    def build_key_to_outvalue(self):
        """
        Build key_to_outvalue dictionary
        Returns
        -------

        """
        self.key_to_outvalue = dict(
            zip(
                [k for k, _ in self.key_to_category.items()],
                [self.category_to_outvalue[v] for _, v in self.key_to_category.items()],
            )
        )


@log_start_completed
def build_default_reclassifiers(reclass_rules: ReclassRules) -> List[ReClassifier]:
    """
    Generate list with the two default reclassifier objects

    Parameters
    ----------
    arguments

    Returns
    -------

    """

    default_newvalue = DefaultReClassifierNewvalue()
    default_newvalue.build(reclass_rules=reclass_rules)

    default_firstrule = DefaultReClassifierFirstrule()
    default_firstrule.build(reclass_rules=reclass_rules)

    return [default_newvalue, default_firstrule]


@log_start_completed
def build_downstream_reclassifiers(
    reclassifiers: List[ReClassifier], arguments: dict, reclass_rules: ReclassRules
) -> List[ReClassifier]:
    """
    Build Model object for all downstream models.

    Parameters
    ----------
    reclassifiers: list of ReClassifiers.
    arguments:
    reclass_rules: ReclassRules object.

    Returns
    -------

    """

    if arguments["firstrule_models"]:
        for column in list(reclass_rules.rules_df):
            if column.startswith("_"):
                reclassifier = DownstreamReClassifier(
                    name=column,
                    key="firstrule",
                    definition_df=reclass_rules.rules_df.loc[
                        reclass_rules.applicable_rules
                    ],
                )
                reclassifier.build_key_to_category()
                reclassifier.build_category_to_outvalue(
                    excel_source=arguments["upstream_excel"]
                )
                reclassifier.build_key_to_outvalue()
                reclassifiers.append(reclassifier)

    if arguments["downstream_sheet"]:
        downstream_sheet = pd.read_excel(
            io=arguments["downstream_excel"],
            sheet_name=arguments["downstream_sheet"],
        )
        for column in list(downstream_sheet):
            if column not in ["newvalue", "description"]:
                reclassifier = DownstreamReClassifier(
                    name=column, key="newvalue", definition_df=downstream_sheet
                )
                reclassifier.build_key_to_category()
                reclassifier.build_category_to_outvalue(
                    excel_source=arguments["downstream_excel"]
                )
                reclassifier.build_key_to_outvalue()
                reclassifiers.append(reclassifier)

    return reclassifiers


@log_start_completed
def evaluate_reclassifiers(
    reclassifiers: List[ReClassifier], combined_raster: CombinedRaster
):
    """
    Calculate area in ha for each category in each model.
    Parameters
    ----------
    downstream_models
    combined_raster

    Returns
    -------

    """

    logger = get_logger()

    for reclassifier in reclassifiers:
        reclassifier.calculate_areas(combined_raster=combined_raster)
        logger.info(
            f"calculated area for {reclassifier.name}: {isinstance(reclassifier.area_df, pd.DataFrame)}"
        )

    return reclassifiers
