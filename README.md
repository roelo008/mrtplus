# Multi Reclass Tool (MRT)

* Door: Hans Roelofsen (WEnR team Biodiversiteit & Beleid)
* Idee en eerste prototype: Henk Meeuwsen (WEnR team Biodiversiteit & Beleid)
* Begonnen: oktober 2020 
* Herontwikkeling met toegevoegde features: augustus 2023

## Inhoud
* [Achtergrond en aanleiding van de MRT](#achtergrond-en-aanleiding-van-de-mrt)  
* [Terminologie](#terminologie)  
* [Theorie en ontwerp van de MRT](#theorie-en-ontwerp-van-de-mrt)  
  *  [Het combined-raster](#het-combined-raster)  
  *  [Beslisregels uitgelegd](#beslisregels-uitgelegd)  
  *  [Beslisregels toegepast](#beslisregels-toegepast)  
  *  [Bruto en netto areaal](#bruto-en-netto-areaal)  
  *  [Downstream: doorvertaling van de nieuwe categorien](#downstream-doorvertaling-van-de-nieuwe-categorieen)  
* [Toepassing van de MRT](#toepassing-van-de-mrt)  
  *  [Installatie](#installatie)  
  *  [Creeer een combined-raster](#creeer-een-combined-raster)  
  *  [Opstellen beslisregels](#opstellen-beslisregels)  
  *  [Command Line Interface](#command-line-interface)  
  *  [MRT Output](#mrt-output)  
* [Ontwikkeling](#ontwikkeling) 
* [Contact](#contact)  

## Achtergrond en aanleiding van de MRT
[WENR](https://www.wur.nl/nl/Onderzoek-Resultaten/Onderzoeksinstituten/Environmental-Research.htm) ontwikkelt en onderhoudt een portfolio aan ecologische modellen, zoals het [MNP model](https://opengis.wur.nl/MNP/). Veel modellen gebruiken GIS kaarten als input. Meestal zijn dit [raster-kaarten](https://desktop.arcgis.com/en/arcmap/10.3/manage-data/raster-and-images/what-is-raster-data.htm) waarin bepaalde (landgebruiks-)categorieen zijn aangeven. Sommige veelgebruikte rasters, zoals de Top10NL en de SNL [Beheertypenkaart](https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/natuurtypen/), zijn door WENR voorbewerkt tot raster-kaarten via het [Basisbestand Natuur & Landschap](https://research.wur.nl/en/publications/basisbestand-natuur-en-landschap).  

Regelmatig blijkt dat een kaartlaag onvoldoende informatie bevat voor de gevraagde toepassing. Dit kan betrekking hebben op het aantal categorieen in de kaart en de omschrijvingen daarvan, of de vorm en omvang van de ruimtelijke contouren in de kaart. Een mogelijke oplossing hiervoor is twee of meer raster-kaarten te 'stapelen'. Binnen de stapel kaarten worden specifieke combinaties van categorieen geidentificeerd die de gewenste informatie verschaffen.  

Een simpel voorbeeld is de combinatie van de Top10NL kaart met de SNL Beheertypenkaart. Eerstgenoemde onderscheidt naald-, loof- en gemengde bossen, laatstgenoemde onderscheidt natuur- en productiebos. In hun combinatie verschijnen zes nieuwe categorieen: 

1. natuur-loofbos
2. gemengd natuurbos
3. natuur-naaldbos
4. productie-loofbos
5. gemengd productiebos
6. productie-naaldbos

De MRT is ontwikkeld om dit proces herhaaldelijk en gemakkelijk toe te passen. Het hoofddoel van de MRT is produceren van nieuwe raster-kaarten waar de nieuwe categorieen in staan. De MRT standaardprocedure is: 

1. identificeer de gewenste bronkaarten
2. combineer de bronkaarten met de [WENR Combine tool](https://git.wur.nl/roelo008/combiner)
3. formuleer de nieuwe categorieen en bijbehorende beslisregels 
4. activeer de MRT en genereer een nieuwe raster-kaart met de nieuwe categorieen.

In dit document wordt de theorie, ontwerp en toepassing van de MRT nader toegelicht. 


## Terminologie
* **raster**: geospatial [raster bestand](https://desktop.arcgis.com/en/arcmap/10.3/manage-data/raster-and-images/what-is-raster-data.htm) zoals gebruikelijk in GIS toepassingen. Tenzij anders vermeld wordt altijd een *categorisch* raster bedoelt (ie *nominaal*, *discreet*) en nooit een *continue* raster.
* **categorie**: een van de discrete categorieen in een categorisch raster. Refereert in MRT context wordt vrijwel altijd naar een *landgebruikscategorie*.
* **value**: een getal in een raster. Elke value in een raster refereert naar een bepaalde **categorie**.
* **description**: de *omschrijving* van een landgebruikscategorie, zoals  zoals bos, water, stad etc.
* **legenda**: de koppeling tussen **values** en **description** voor een *categorisch* raster.   
* **categorisatie**: alle categorieen binnen een raster 
* **combined-raster**: output van de [WENR Combine tool](https://git.wur.nl/roelo008/combiner). 
* **bron-raster**: een van de rasters die is ingevoerd in de WENR Combine tool. 
* **combined-value**: een integer getal in het *combined-raster* dat symbool staat voor een bepaalde combinatie van categorieen in alle *source rasters*. 
* **RAT** of **VAT**: [Raster (of Value) Attribute Table](https://desktop.arcgis.com/en/arcmap/latest/manage-data/raster-and-images/raster-dataset-attribute-tables.htm). Een tabel waarin de *values* van een raster worden geassocieerd met aanvullende informatie, zoals een *description*. In de *RAT* van een *combi-raster* staan de *unique integer values assigned to each unique combination of input values* ([ESRI](https://pro.arcgis.com/en/pro-app/latest/tool-reference/spatial-analyst/combine.htm)).
* **beslisregel**: een bepaalde combinatie van *categorieen* in een of meerdere *source-rasters* die resulteert in een nieuwe *categorie*. 
* **newvalue**: de raster *value* die een *reclass-rule* toekent aan  *categorie* behorende bij de *reclass-rule*
* **upstream**: het geheel aan *reclass-rules* die tezamen het *combined-raster* vertalen naar een output raster met *newvals* en bijbehorende *descriptions*.
* **downstream**: doorvertaling van *newval* kaart naar een andere categorisatie, waarbij *newvalue* categorieen samengeraapt kunnen worden tot een enkele *downstream* categorie. 
* **firstrule**: de *reclass-rule* die als eerste van toepassing is op een pixel in het *combined-raster* en wiens *newval* wordt toegekend aan die pixel. 

## Theorie en ontwerp van de MRT

### Het combined-raster
Een combined-raster is een raster waarin de values verwijzen naar specifieke value-combinaties van een _n_ aantal bronrasters. 
Hieronder wordt het principe van een *combined raster* geillustreerd aan de hand van twee categorische rasters: Beheertypen en Top10. De beheertypen legenda is:

|value|description|
|------|-----|
|10|N08.02 Open duin
|11|N16.03 droog bos met productie|
|12|N16.04 vochtig bos met productie|
|13|N17.02 Drooghakhout|

De legenda van de Top10 kaart is:

|value|description|
|------|-----|
|100|grasland|
|142|gemengd bos|
|143|loofbos|
|144|naaldbos|
|160|duin|
|161|duin met riet|

In de figuur hieronder zijn de beheertypen- en Top10 rasters weergegeven (links en midden). Rechts staat het *combined_raster*. De getallen in het *combined_raster* verwijzen naar dezelfde combinaties van waarden in de twee bronrasters.

![combined_raster](mrt/resources/demo_combined_raster.JPG)

De RAT van het *combined_raster* is dan: 

|value|beheertypen|top10|
|-----|-----|-----|
|1|10|160|
|2|10|100
|3|10|143|
|4|11|142|
|5|11|143|
|6|11|144|
|7|11|100|
|8|12|161|
|9|13|144|

De getallen in het *combined_raster* verklaren de combinaties van getallen in de twee bronrasters. Dit voorbeeld gebruikt twee bronrasters, maar in de praktijk worden meerdere bronrasters gecombineerd. 

#### Beslisregels uitgelegd
_in dit stuk vind ik de losse tabelletjes erg verwarrend, weet niet waar ik precies moet kijken. Ik zou gewoon 1 tabel geven en daar dan naar verwijzen_
Beslisregels identificeren gewenste categorieen uit een of meerdere bronrasters en koppelen daaraan een nieuwe categorie. De nieuwe categorie wordt gekenmerkt door een numerieke waarde (newvalue) en een omschrijving (description). 

Hieronder worden drie beslisregels als voorbeeld gegeven, gebaseerd op de twee eerder genoemde bronrasters. Het doel is om beheertype `N16.03 droog bos met productie` te verfijnen naar loof-, naald- en gemengd bos, gebaseerd op Top10 informatie. 

|newvalue|description|remark|volgnr|Beheertypen|Top10|
|------|------|------|-----|-----|-----|
| 10 | Gemengd droog bos met productie| |1|11 |142|
| 11 | Droog loofbos met productie | |2|11 | 143|
| 12| Droog naalbos met productie | | 3| 11| 144| 

De beslisregels zijn opgebouwd uit de volgende componenten:

1. *newvalue*: de waarde (value) van de nieuwe categorie in het resulterende raster
2. *description*: de omschrijving van de nieuwe cateogire
3. *remark* (optioneel): ruimte voor de gebruiker om de beslisregel nader toe te lichten
4. *volgnr*: nummer ter identificatie van de beslisregel
5. *Top10*: de gewenste categorieen uit het Top10 bronraster
6. *beheertypen*: de gewenste categorieen uit het Beheertypen bronraster.

Het is mogelijk dat `N16.03 droog bos met productie` niet altijd samenvalt met loof-, naald- of gemengd bos uit de Top10. Beslisregel #4 identificeert die plekken, door te stellen dat de Top10 value niet belangrijk is. Dit wordt aangeduid met een `n`.

| newvalue |description|remark|volgnr|Beheertypen|Top10|
|----------|------|------|-----|-----|-----|
| 13       | Overig droog bos met productie| |4|11|n|

Beslisregels worden van boven naar beneden toegepast: zodra een locatie in het raster is toegekend aan een beslisregel wordt die niet meer overschreven door een lagere beslisregel. Dit geldt voor beslisregel #4, die van toepassing is op _alle_ gebieden met N16.03. Beslisregels #1-3 zijn echter _specifieker_, of met ander woord _strenger_, omdat ze een aanvullende eis in de Top10 kaart bevatten. Omdat deze eerder worden toegepast, resteert voor beslisregel #4 alleen  N16.03 buiten Top10 loof-, naald- en gemengd bos. Hierom is het belangrijk om *strengere* *beslisregels* te prioriteren boven *lossere* regels.

Meerdere *beslisregels* kunnen leiden tot dezelfde *newvalue*, bijvoorbeeld als informatie over dezelfde categorie afkomstig is uit verschillende *bronrasters*. Beslisregel #5 identificeert duinen uit Top10 en beslisregel #6 identifeert duinen uit de Beheertypenkaart. Beiden resulteren in dezelfde categorie: 'Duinen' (aangeduid met value 14).

|newvalue|description|remark|volgnr|Beheertypen|Top10|
| ------ | ------ | ------ |------ | ----- |-----|
| 14 | Duinen |duinen Top10|5| n| 160,161 |
| 14 | Duinen | duinen uit beheertypen|6| 10 | n |

Als beslisregels #5 en #6 gecombineerd zouden worden in een enkele beslisregel, geeft dat een wezenlijk ander resultaat:

|newvalue|description|remark|volgnr|Beheertypen|Top10|
| ------ | ------ | ------ |------ | ----- |-----|
| 14 | Duinen | |7|10 | 160,161 |

Als er meerdere categorieen binnen een bronraster gespecifeerd worden, beschouwt de MRT deze als optioneel ten opzichte van elkaar. `160, 161` voor Top10 in beslisregel #5 wordt geinterpreteerd als _160 *OF* 161_. Categorieen uit verschillende bronrasters zijn 'dwingend' ten opzichte van elkaar. Beslisregel #1 wordt dus geinterpreteerd als _142 uit Top10 *EN* 142 uit beheertypen_.

Dezelfde `newvalue` moet altijd gekoppeld zijn aan dezelfde `description`. De MRT controleert hierop en geeft feedback waar nodig. Tevens controleert de MRT of de opgegeven values in de de beslisregel, daadwerkelijk voorkomen in het betreffende bronraster. Als dat niet het geval is, wordt de MRT gestopt. 

De beslisregels worden aan de MRT aangeboden via een Excel-sheet. Het betreffende tabblad wordt vaak aangeduid als het `upstream` gedeelte van de MRT. 


#### Beslisregels toegepast
De MRT vertaald elke beslisregel naar een database-query die wordt toegepast op de RAT van het *combined_raster*. Voor alle beslisregels ("regel") wordt hieronder aangegeven of het *combined_raster* voldoet aan de query (1/0).

|value|beheertypen|top10|regel01|regel02|regel03|regel04|regel05|regel06|regel07
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|1|10|160|0|0|0|0|1|1|1
|2|10|100|0|0|0|0|0|1|0
|3|10|143|0|0|0|0|0|1|0
|4|11|142|1|0|0|1|0|0|0
|5|11|143|0|1|0|1|0|0|0
|6|11|144|0|0|1|1|0|0|0
|7|11|100|0|0|0|1|0|0|0
|8|12|161|0|0|0|0|1|0|0
|9|13|144|0|0|0|0|0|0|0

Vervolgens voegt de MRT 'beslisregel #999' toe die van toepassing is op _alle_ waarden in het *combined_raster*. Dit is de terugval optie als geen van de door de gebruiker opgegeven beslisregels van toepassing is. Tenslotte bepaalt de MRT welke belisregel als eerste (dwz de meest linker) van toepassing was. Dit wordt vastgelegd in een nieuwe kolom `first_rule`.


|value|beheertypen|top10|regel01|regel02|regel03|regel04|regel05|regel06|regel07|regel999|first_rule|
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|1|10|160|0|0|0|0|1|1|1|1|regel05
|2|10|100|0|0|0|0|0|1|0|1|regel06
|3|10|143|0|0|0|0|0|1|0|1|regel06
|4|11|142|1|0|0|1|0|0|0|1|regel01
|5|11|143|0|1|0|1|0|0|0|1|regel02
|6|11|144|0|0|1|1|0|0|0|1|regel03
|7|11|100|0|0|0|1|0|0|0|1|regel04
|8|12|161|0|0|0|0|1|0|0|1|regel05
|9|13|144|0|0|0|0|0|0|0|1|regel999

De vertaling van het *combined_raster* naar het output raster verloopt dan volgens onderstaande tabel (waarbij `0` wordt aangehouden als `newalue` voor beslisregel #999):

|combined_value|newvalue|
|-----|-----|
|1|14
|2|14
|3|14
|4|10
|5|11
|6|12
|7|13
|8|14
|9|0

De kerntaak van de MRT is om het combined-raster te vertalen volgens bovenstaande tabel en het resultaat weg te schrijven naar een nieuw rasterbestand. Het resulterende raster wordt aangeduid als het `newval` raster. Daarnaast is de MRT in staat om een nieuw raster te maken waarin niet de `newval`, maar de van-toepassing-zijnde beslisregel staat aangegeven. Dit is het `firstrule` raster. _Ik vind dit een verwarrende term, ik zou het zelf eerder applicable_rule noemen of iets in die richting_

### Bruto en netto areaal
De RAT van het combined_raster heeft ook een `Count` kolom, waarin het aantal pixels met die pixelwaarde staan. Gegeven de oppervlakte van _een_ pixel, kan deze informatie eenvoudig omgezet worden naar een areaal in hectare. De RAT in dit voorbeeld ziet er dan als volgt uit (veronderstelende een oppervlakte van 1 per pixel):

|value|beheertypen|top10|oppervlak|
|-----|-----|-----|-----
|1|10|160|2
|2|10|100|2
|3|10|143|1
|4|11|142|3
|5|11|143|1
|6|11|144|2
|7|11|100|1
|8|12|161|1
|9|13|144|1

Met deze informatie kan het bruto en netto areaal berekend worden dat van toepassing is op elke beslisregel. 

|newvalue|description|remark|volgnr|Beheertypen|Top10|bruto|netto|verschil
|------|------|------|-----|-----|-----|-----|-----|-----|
| 10 | Gemengd droog bos met productie| |1|11 |142|3|3|0
| 11 | Droog loofbos met productie | |2|11 | 143|1|1|0
| 12| Droog naalbos met productie | | 3| 11| 144|1|1 |0
| 13 | Overig droog bos met productie| |4|11|n|7|1|6
| 14 | Duinen | |5| n| 160,161 |3|3|0
| 14 | Duinen | |6| 10 | n |5|3|2
| 14 | Duinen | |7|10 | 160,161 |2|0|2
|0|overig| | 999| - | - |14 |1|13

Het bruto areaal is datgene waar de beslisregel in beginsel op van toepassing is, voordat beslisregels met hogere prioriteit worden toegepast. Het netto areaal is datgene waar de beslisregel uiteindelijk van toepassing is; de _firstrule_. Per definitie is het netto areaal kleiner of gelijk aan het bruto areaal en zijn bruto- en netto gelijk voor de beslisregel met hoogste prioriteit. 

Netto arealen kunnen gesommeerd worden per nieuwe categorie:

|newvalue|description|netto
|-----|-----|-----|
|10|Gemengd droog bos met productie|3
|11|Droog loofbos met productie|1
|12|Droog naaldbos met productie|1|
|13|Overig Droog bos met productie|1|
|14|Duinen|6


### Downstream: doorvertaling van de nieuwe categorieen
Beslisregels resulteren in nieuwe categorieen. Het kan wenselijk zijn om die vervolgens te vertalen naar een andere categorisatie. Dit proces wordt aangeduid als 'downstream' (ie benedenstrooms, of *afgeleid* van de nieuwe kaart). Downstream categorisaties kunnen opgesteld worden voor zowel de `newvalues` van de nieuwe categorieen, of op de individuele beslisregels (de `firstrule`). Hieronder worden beide situaties toegelicht.

#### volgens newvalue
In onderstaand voorbeeld worden de categorieen van belisregels #1-7 doorvertaald naar twee downstream-modellen: 'algemeen_landgebruik' en 'bostypen'.

| newvalue | description | algemeen_landgebruik| bostype | 
| ------ | ------ | ------ |------ |
| 10 | Gemengd droog bos met productie | Bos | Gemengd|
| 11 | Droog Loofbos met productie | Bos | Loof | 
| 12 | Droog naaldbos met productie | Bos | Naald | 
| 13 | Overig droog bos met productie | Bos | Gemengd |
| 14| Duinen | Zand | |

Downstream modellen hoeven niet elke `newvalue` te adresseren: `bostypen` blijft leeg voor `newvalue` 14. Dit resulteert in een `NoData`.

De *downstream* regels staan in een apart Excel tabblad. Hierin staan doorgaans minder regels in dan in het *upstream* tabblad: elke *newvalue* wordt immers eenmalig benoemt in *Downstream*. Voor elke kolom in het *downstream* tabblad kan de MRT een kaart produceren.

In het *Downstream* tablad worden de categorie *descriptions* ingevuld in plaats van numerieke *values* (zoals in het *upstream* tabblad). De *value* die wordt toegekend aan elke categorie in een *downstream-model* (ie `Gemengd` en `Loof` voor `bostypen`) kan gedefinieerd worden in een Excel tabblad met de naam van het *model*: 

|Description|Value|
|-----|-----|
|Gemengd|141|
|Loof|142|
|Naald|143|

De gebruiker is vrij om nieuwe values te kiezen voor de downstream categorieen. In dit voorbeeld wordt voor `bostypen` de values van Top10 gereconstrueerd. Categorieen in downstream modellen zonder bijbehorend tabblad worden automatisch genummerd vanaf 1, zoals in dit geval voor `algemeen_landgebruik`.

|Description|Value|
|---|---|
|Bos|1|
|Zand|2|


De MRT kan voor elk downstream-model een nieuw raster wegschrijven. Dit gebeurt door de _combined\_value_ te koppelen aan de values van het downstream model. Voor bijvoorbeeld het `bostype` model verloopt dat volgens onderstaande tabel:

|combined\_value|bostype\_value|
|-----|-----|
|1|0
|2|0
|3|0
|4|141
|5|142
|6|143
|7|141
|8|0
|9|0
 

#### volgens firstrule
Het kan wenselijk zijn om dezelfde *newvalue* anders te behandelen in de *downstream*, bijvoorbeeld om de beslisregels te groeperen naar een bepaalde gemeenschappelijke deler. Hiertoe kunnen *downstream-modellen* toegevoegd worden als extra kolommen aan de *upstream* sheet (deze kolomnamen moeten beginnen met een `_`). In onderstaande tabel wordt een *downstream model* `_oorsprong` gedefinieerd. _Moeten downstram modellen altijd met \_ beginnen?_

|newvalue|description|remark|volgnr|Beheertypen|Top10|_oorsprong
|------|------|------|-----|-----|-----|-----|
| 10 | Gemengd droog bos met productie| |1|11 |142|top10&SNL
| 11 | Droog loofbos met productie | |2|11 | 143|top10&SNL
| 12| Droog naalbos met productie | | 3| 11| 144|top10&SNL
| 13 | Overig droog bos met productie| |4|11|n|SNL
| 14 | Duinen | |5| n| 160,161 |top10
| 14 | Duinen | |6| 10 | n |SNL
| 14 | Duinen | |7|10 | 160,161 |top10&SNL

Beslisregels #5-7 resulteren volgens bovenstaand schema in andere categorieen voor `_oorsprong`, ondanks hun gedeelde `newvalue`. Net als bij *downstream volgens newval* kunnen de numerieke waardes de categorieen in het `_oorsprong` model gedefinieerd worden in een apart tabblad. Als dat niet gebeurt, kent de MRT automatisch nummers toe. 

Voor het \_oorsprong downstream-model kan de MRT een nieuw raster wegschrijven, gebaseerd op onderstaande tabel (veronderstellende dat de _oorsprong categorieen automatisch genummerd worden vanaf 1):

|combined\_value|\_oorsprong\_value|
|-----|-----|
|1|1
|2|1
|3|1
|4|1
|5|1
|6|1
|7|2
|8|3
|9|0

Op dezelfde wijze zoals eerder beschreven, kan voor elke categorie in een downstream model het bruto- en netto areaal bepaald worden. 


## Toepassing van de MRT
Hieronder volgt instructie om de MRT te installeren, de benodigde bestanden te bereiden en om de MRT te gebruiken. 

### Installatie
1. clone deze repository naar een lokale directory (bijv `c:/apps/mrt`) mbv git: `git clone  git@git.wur.nl:roelo008/mrt.git`. WUR-gebruikers kunnen een [ssh](https://docs.gitlab.com/ee/user/ssh.html) key toevoegen aan hun wur-gitlab account.
2. creeer een nieuw python environment, of activeer een bestaande. 
3. navigeer naar `c:/apps/mrt`
3. typ `pip install .` 

Optioneel kan de MRT repository gedownload worden vanaf git.wur.nl, in dat geval is installatie van git niet nodig.

### Creeer een combined-raster
Om daadwerkelijk met de MRT aan de slag te gaan, is de eerste stap het vezamelen van alle bronrasters. Deze moeten van integer data-type en identiek vormgegeven zijn:
* aantal rijen en kolommen
* pixel groote (ie resolutie)
* oorsprong in geprojecteerd coordinaten-referentiesysteem.

Een veelgebruikt format voor rasters binnen WEnR is:

* 2.5m resolutie
* 130.000 rijen, 112.000 kolommen
* extent in RD-New ([epsg:28992](https://epsg.org/crs_28992/Amersfoort-RD-New.html)):
    * left: 0 m
    * bottom: 300.000 m
    * right: 280.000 m
    * top: 625.000 m

Gebruik de [WENR COMBINE tool](https://git.wur.nl/roelo008/combiner) om alle bronrasters te combineren in _een_ combined raster. Het output-format moet `gdal` compatible zijn; in de praktijk is `GeoTiff` het beste format. Pas LZW compressie toe  om de bestandsgrootte te beperken. De output ziet er dan uit als:
* `combi_raster_v1.tif`: het raster zelf
* `combi_raster_v1.csv`: raster attribute table


### Opstellen beslisregels
De *beslisregels* staan in een Excel document tabblad met de volgende kolommen:

| newvalue | description |remark|reclass|volgnummer|<bronraster_1>|<bronraster_2>|<bronraster_n>|
|----------|-------------|-----|-----|-----|-----|-----|-----|

De MRT controleert of de *bronraster_* kolomnamen corresponderen met de kolomnamen in de *combined-raster RAT* en geeft feedback waar nodig. 

Maak een *reclass-rule* per regel in de excel:
* `newvalue`: het getal dat de nieuwe landgebruikscategorie vertegenwoordigt
* `description`: de  omschrijving van de nieuwe categorie
* `remark`: ruimte voor opmerkingen
* `reclass` staat op `1` als de *reclass-rule* toegepast moet worden, of op `0` als de *reclass-rule* genegeerd moet worden
* `volgnummer`: nummer ter identificatie van de beslisregel. Wordt niet gebruikt in de MRT.
* in elke *source-raster* kolom
    * de waarde(s) uit dat bron-raster die *wel* meedoen (_een_ of meerdere hele getallen, komma gescheiden als meer dan _een_). Bijvoorbeeld: `101` of `101, 102, 103`
    * OF welke waardes uit het *source-raster* *niet* meedoen (_een_ of meerdere hele getallen, komma gescheiden en voorafgegaan met  `-`). Bijvoorbeeld: `-101` of `-101, 102, 103, 104`
    * OF een range aan waardes die meedoen. Noteer het begin- en eindgetal, gescheiden door een `*`. Bijvoorbeeld: `100*110`.
    * OF indifferentie naar dat *source-raster*, aangeduid met `n`

### Command Line Interface
De MRT wordt aangestuurd via de Command Line. Er zijn drie vereiste parameters en diverse optionele. 

```commandline
positional arguments:
  xlsx                  path to Excel file with reclass rules
  raster                path and filename to combined raster
  us                    name of upstream Excel sheet

options:
  -h, --help            show this help message and exit
  --ds_sheet            name of downstream Excel sheet
  --ds_xlsx             path to Excel file with downstream sheet (defaults to `xlsx`)
  --report              write report to file
  --out_dir             output directory (default ./)
  --basename            output file basename (default: Reclass Excel filename)
  --ts                  add timestamp to basename

--write_raster:
  --target              Write what to raster?
  --of {GTiff,Ehdr}     Output file format (default: Gtiff).
  --xtnt:               Target extent: left bottom right top.
  --tr                  Target resolution in m (default: equal to input raster).
  --r {mode,nearest,sum,average,max,min}
                        Resample method (default: mode) if --tr > combined raster resolution
  --nodata              Set a value as NoData (default: 0).
  --mnp_legend          Write MNP style CSV table.
  --testing             Test configuration. Writes 0.2% of combined raster to output file
```

Onderstaand voorbeeld draait de MRT en creëert een nieuw raster met daarin de nieuwe landgebruik categorieën (aangeduid als 'newvalue') en een rapport waar met samenvatting van de reclass-regels en de _downstream_ modellen. 

`python -m mrt c:/scenario_reclass.xlsx c:/combinatie_raster.tif upstream_sheet --report --target newvalue --base_out c:/apps/output --ds_sheet downstream`

In het volgende voorbeeld wordt de extent van de output bijgesneden tot de extent van de provincie Drenthe: 

`python -m mrt c:/scenario_reclass.xlsx c:/combinatie_raster.tif upstream_sheet --target newvalue --xtnt 200000 510000 270000 580000`

Hieronder wordt de resolutie van het output raster veranderd ten opzichte van de resolutie van het _combined\_raster_. De [resampling](https://rasterio.readthedocs.io/en/latest/topics/resampling.html) methode is `mode`, wat betekent dat de meest voorkomende waarde in de originele (kleine) pixels wordt overgenomen in de nieuwe (grotere) pixel. Dit is voor de hand-liggende voor categorische data. Andere resamplings zijn ook mogelijk, zoals `sum`. Dit kan nuttig zijn om bijvoorbeeld binaire rasters te sommeren.

`python -m mrt c:/scenario_reclass.xlsx c:/combinatie_raster.tif upstream_sheet --target newvalue --tr 10 --r mode`

De downstream Excel sheet mag aangeboden worden in een aparte Excel: 

`python -m mrt c:/scenario_reclass.xlsx c:/combinatie_raster.tif upstream_sheet --report --base_out c:/apps/output --ds_sheet downstream --ds_xlsx c:/downstream_alle_scenarios.xlsx`
 
Potentiele fouten in de Excel(s) worden ondervangen en teruggelegd aan de gebruiker. Als er geen *downstream* sheet wordt gespecificeerd voor de MRT, kunnen enkel de *newvalue* en *firstrule* modellen weggeschreven als nieuw raster.   

### MRT Output
Hieronder worden de producten omschreven die de MRT kan produceren. De daadwerkelijke output van de de MRT is afhankelijk van de opdracht van de gebruiker. 

#### Rasters
De MRT kan nieuwe rasterbestanden produceren. In het geval alleen een `upstream` sheet is opgegeven, kan de MRT het `newvalue` of het `firstrule` raster produceren. Als de beslisregels zijn uitgebreid met downstream modellen volgens de `firstrule`, dan kunnen deze downstream modellen ook geproduceerd worden. De rasters die de MRT produceert bestaan uit de volgende bestanden:

* \<basename\>\_<model_name>.tif: het GeoTiff bestand met het daadwerkelijke raster
* \<basename\>\_<model_name>.qml: QGIS layer style file. In dit bestand staan de legenda en kleurstelling van de nieuwe categorieen Als het *.tif bestand in QGIS wordt geopend, wordt dit bestand automatisch herkend en toegepast.
* \<basename\>\_<model_name>.tif.vat.dbf: de raster attribute table (RAT). Dit is een digitale tabel waarin de koppeling tussen de rastervalue en de categorie is vastgelegd. Dit bestand wordt automatisch herkend door ArcGIS Pro en door QGIS mits de
juiste [plugin](https://github.com/noaa-ocs-hydrography/qgis-raster-attribute-table-plugin) is geïnstalleerd

De _basename_ van MRT producten is gelijk aan de de bestandsnaam van de Excel met de beslisregels, gevolgd door de datum & tijdstip waarop dit bestand voor het laatst gewijzigd is. De _basename_ kan ook handmatig worden ingesteld met het `--basename` CLI argument. _model\_name_ verwijst naar het 'model' dat de MRT heeft geproduceerd. Dit kan de `newval` of `firstrule` zijn, of een van de downstream modellen. 

#### Rapport
De MRT produceert standaard een Excel rapport als het `newvalue` model wordt weggeschreven, in andere gevallen is het rapport optioneel. Het rapport kan ook gemaakt worden als er geen rasters worden geproduceerd. Het rapport bevat de volgende tabbladen.

##### metadata
Hierin zijn alle details van de MRT run vastgelegd, zoals de gebruikte bronbestanden en de datum-tijdstip van de run.  

##### ReclassRules
De beslisregels worden hier nogmaals genoemd, zoals ze zijn opgegeven door de gebruiker. Daarnaast zijn extra kolommen toegegoegd:

* mrt_query: de formele database query die wordt toegepast op de combined\_raster RAT
* name: naam van de beslisregel
* priority: numerieke indicatie van de prioriteit van de beslisregel, waarbij 0 de hoogste prioriteit heeft
* gross\_match\_ha: bruto areaal in hectare van het combined_raster dat voldoet aan de beslisregel. 
* net\_match\_ha: netto areaal in hectare van het combiend\_raster dat voldoet aan de beslisregel.
* difference: verschil tussen bruto en netto areaal. 

Het bruto areaal van een beslisregel slaat op het areaal dat voldoet aan de beslisregel, voordat hogere-prioriteit beslisregels worden toegepast. Het netto-areaal is het areaal dat resteert na toepassing van hogere-prioriteit beslisregels. Het netto areaal van een beslisregel is daarom nooit kleiner dan het bruto areaal. 

De bruto, netto en verschil kolommen helpen de gebruiker te verifieren dat de beslisregels naar verwachting werken. Een netto-areaal van 0 hectare kan bijvoorbeeld betekenen dat de beslisregel te laag ge-prioriteerd is, of te 'streng' is. 

##### Reclassed
In dit tabblad staat de RAT van het combined_raster. Voor elke waarde is aangegeven aan welke `newval` het is toegekend. Wegens beperkingen van Excel wordt dit tabblad alleen aangemaakt als de RAT niet meer dan 1.048.576 rijen bevat.

##### source\_raster<naam>
Voor elk bronraster wordt een apart tabblad aangemaakt waarin de values genoemd worden, plus areaal daarvan.

#### mnp_tabel
Optioneel maakt de MRT een *.csv bestand waarin de legenda van het geproduceerde raster staat. De legenda is opgemaakt volgens de vereisten van de [land type raster legenda](https://opengis.wur.nl/MNP/contents/data_specificaties.html#land-type-raster-legenda-tabel) van het MNP model. Deze optie is expliciet bedoeld voor rasters met SNL Beheertypen.

## Ontwikkeling
* v1.0 developed: october 2020 retired: sept 2023  
* v2.0 developed: mid 2023. 

Geplande ontwikkelingen: 
 * unit-tests
 * sample-dataset

## Contact 
[Hans Roelofsen](https://www.wur.nl/en/Persons/Hans-dr.-HD-Hans-Roelofsen.htm).

## Copyright
CC BY-NC-ND 4.0 [Summary](https://creativecommons.org/licenses/by-nc-nd/4.0/) [Full text](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)


